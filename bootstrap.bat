if defined ProgramFiles(x86) (
    set appDir="%ProgramFiles(x86)%\Agente Admintotal"
    set pythonPath="%ProgramFiles(x86)%\Python37"
    set pythonExe="%ProgramFiles(x86)%\Python37\python.exe"
) else (
    set appDir="%ProgramFiles%\Agente Admintotal"
    set pythonPath="%ProgramFiles%\Python37"
    set pythonExe="%ProgramFiles%\Python37\python.exe"
)

@ECHO OFF
start cmd.exe /C "%pythonExe% %appDir%\bootstrap.py"
