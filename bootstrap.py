import django
from django.core import management
import threading
import os
import subprocess

PORT = os.environ.get("HTTP_PORT", 8000)


class StoppableThread(threading.Thread):
    """Thread class with a stop() method. The thread itself has to check
    regularly for the stopped() condition."""

    def __init__(self,  *args, **kwargs):
        super(StoppableThread, self).__init__(*args, **kwargs)
        self._stop_event = threading.Event()

    def stop(self):
        self._stop_event.set()

    def stopped(self):
        return self._stop_event.is_set()

def runserver():
    # subprocess.call(["waitress-serve", f"--listen=*:{PORT}", "--threads=8", "agente.wsgi:application"])
    management.call_command("runserver", f"127.0.0.1:{PORT}", "--noreload")

def start_agent():
    management.call_command("agente", "iniciar")

if __name__ == "__main__":
    os.environ['DJANGO_SETTINGS_MODULE'] = 'agente.settings'
    django.setup()

    try:
        management.call_command("migrate")
        thread_runserver = StoppableThread(target=runserver)
        thread_runserver.start()
        start_agent()
    except Exception as e:
        error_text = str(e)
        print(error_text)
            