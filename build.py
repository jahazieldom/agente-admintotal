import subprocess

def get_nueva_version(anterior):
    split_version = anterior.split(".")
    n = int(split_version.pop()) + 1
    split_version.append(n)
    return ".".join([str(x) for x in split_version])

version_anterior = open("version").read()
nueva_version = get_nueva_version(version_anterior)
cambios_version = "Comentarios generados de manera automática build.py"

print("")
print("*" * 25)
print(f"Versión anterior: {version_anterior}")
print(f"Nueva version: {nueva_version}")
print("*" * 25)

print("Actualizando versión en el repositorio...")
open("version", "w").write(nueva_version)

print("Creando nuevo tag en el repositorio....")
subprocess.call(["git", "commit", "-am", "Nueva versión generada por build.py"])
subprocess.call(["git", "push"])
subprocess.call(["git", "tag", "-a", f"v{nueva_version}", "-m", cambios_version])
subprocess.call(["git", "push", "origin", "--tags"])

# print("Creando nuevo tag en el repositorio....")
# subprocess.call(["pyinstaller", "scripts/installer.py", "--onefile", "--icon=admintotal.ico"])