# Descargar instalador del agente (Windows)
[Descargar](https://gitlab.com/jahazieldom/agente-admintotal/-/raw/master/dist/Instalador-agente-admintotal.exe?inline=false)

## IMPORTANTE
Deshabilitar la edición rápida de las propiedades (y valores defecto) de la aplicación cmd donde esta corriendo el agente, ya que si no se deshabilita al dar click en cualquier parte de la terminal el proceso se pausea. 

# Requerimientos del sistema
- Python 3.7 de 32 bits es obligatorio !!


## Crear settings.py 
```console 
$ cp agente/settings_copy.py agente/settings.py
```

## Iniciar servicio para consulta de tareas
```console 
$ python manage.py agente iniciar
```

## Generar actualización
Para generar una nueva versión del agente es necesario dar commit y push a los cambios que queremos en la nueva versión, despues corremos este comando para generar la nueva versión:

```console
$ python build.py
```

El instalador se encargará de descargar siempre la última versión publicada en el repositorio (cada versión es un tag en el repositorio)

## Hacer cambios en el instalador de windows
El instalador es un archivo ejecutable para windows que se encuentra en este repositorio, en la ruta `./dist/Instalador-agente-admintotal.exe`, este archivo es generado por el script que se encuentra en la ruta `./scripts/installer.py`, lo cual, se encarga de configurar lo necesario en la máquina que estará corriendo el agente, lo que hace el instalador es:

- Instalar python 3.7.9
- Descargar el código fuente de este repositorio
- Crear un virtualenv
- Instalar `requirements.txt`
- Copiar el archivo `bootstrap.bat` al Startup (`{APP_DATA}\Microsoft\Windows\Start Menu\Programs\Startup`)
- Copiar archivos necesarios como el `settings.py` y el archivo de configuración para impresiones `./lib/PDF-XChange Viewer Settings.dat`

Al hacer una modificación en el script de instalación (`./scripts/installer.py`) debemos de volver a generarlo con el siguiente comando (**ejecutarlo desde la raíz del proyecto**):

```console
$ pip install pyinstaller
$ pyinstaller scripts/installer.py --onefile --name=instalador-agente-admintotal.exe
$ # generar instalador para 64 bits
$ pyinstaller scripts/installer_amd64.py --onefile --name=instalador-agente-admintotal-amd64.exe
```

## Configuración de impresión
Es posible configurar propiedades para las impresiones (margenes y tamaños de papel), esto lo podemos hacer generando un archivo de configuración `./lib/PDF-XChange Viewer Settings.dat`, ese archivo lo podemos generar usando una herramienta llamada **PDF-XChange Viewer** , ahí podremos para previsualizar las configuraciones de impresión, URL de descarga de la herramienta:

https://www.tracker-software.com/product/pdf-xchange-viewer

Una vez exportado el archivo de configuración, podremos adjuntarlo en la configuración principal del agente.

Actualmente se usa la herramienta `pdftoprinter` (http://www.columbia.edu/~em36/pdftoprinter.html) para mandar imprimir PDF's a una impresora en especifico.

**NOTA**

Una vez importadas las configuraciones el archivo deberá llamarse "PDF-XChange Viewer Settings.dat" y deberá ser copiado en la carpeta ./lib/ del proyecto 


## Terminales de banorte
Instalar Visual C++ Redistributable para Visual Studio 2012 (x86)
https://www.microsoft.com/es-mx/download/details.aspx?id=30679

Instalar Visual C++ Redistributable para Visual Studio 2013 (x86)
https://www.microsoft.com/es-mx/download/details.aspx?id=40784

Driver Verifone 
https://soporte.admintotal.com/central_upload/archivos_ticket/Driver_Verifone.rar

# Biometrico
Management command para probar integración con neurotechnology
```console
$ python manage.py neurotec 
$ python manage.py neurotec -m <nombre_metodo>
```