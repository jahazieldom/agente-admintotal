import datetime
from django import forms
from django.contrib.auth.models import User
from django.forms.widgets import ClearableFileInput
from . import functions
from .constants import (
    ELEMENTOS_PAGINA,
    MESES,
    SOLO_FECHA_ANTERIOR,
    SOLO_FECHA_POSTERIOR,
    DESDE_1900,
)

EMPTY_CHOICE = [[None, "--------"]]

class PasswordInput(forms.PasswordInput):
    def __init__(self, *args, **kwargs):
        attrs = kwargs.get("attrs", {})
        attrs["autocomplete"] = "new-password"
        super(PasswordInput, self).__init__(attrs=attrs)


class CustomModelForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        usuario = kwargs.pop("usuario", None)
        super(CustomModelForm, self).__init__(*args, **kwargs)

class CustomForm(forms.Form):
    def __init__(self, *args, **kwargs):
        usuario = kwargs.pop("usuario", None)
        super(CustomForm, self).__init__(*args, **kwargs)

class BootstrapForm(CustomForm):
    def __init__(self, *args, **kwargs):
        super(BootstrapForm, self).__init__(*args, **kwargs)
        for f in self.visible_fields():
            fields_exclude = [
                forms.FileField,
                forms.BooleanField,
                forms.ModelMultipleChoiceField,
            ]
            exclude = [
                True
                for x in fields_exclude
                if isinstance(self.fields[f.name], x)
            ]
            if not exclude:
                css_class = self.fields[f.name].widget.attrs.get("class", "")
                
                if f.name == "banco":
                    css_class += " autocomplete-banco"

                self.fields[f.name].widget.attrs.update(
                    {"class": css_class + " form-control"}
                )
            else:
                self.fields[f.name].widget.attrs.update(
                    {"style":"max-width:100%"}
                )

    def clean(self):
        cleaned_data = super(BootstrapForm, self).clean()
        return cleaned_data


class BootstrapModelForm(CustomModelForm):
    def __init__(self, *args, **kwargs):
        super(BootstrapModelForm, self).__init__(*args, **kwargs)
        for f in self.visible_fields():
            fields_exclude = [
                forms.FileField,
                forms.BooleanField,
                forms.ModelMultipleChoiceField,
            ]
            exclude = [
                True
                for x in fields_exclude
                if isinstance(self.fields[f.name], x)
            ]
            if not exclude:
                css_class = self.fields[f.name].widget.attrs.get("class", "")

                if f.name == "banco":
                    css_class += " autocomplete-banco"
                    
                self.fields[f.name].widget.attrs.update(
                    {"class": css_class + " form-control"}
                )
            else:
                if isinstance(self.fields[f.name], forms.BooleanField):
                    css_class = self.fields[f.name].widget.attrs.get("class", "")
                    self.fields[f.name].widget.attrs.update(
                        {"class": css_class + " form-check-input"}
                    )

                self.fields[f.name].widget.attrs.update(
                    {"style":"max-width:100%"}
                )

    def add_method(self, method, name=None):
        if name is None:
            name = method.func_name
        setattr(self.__class__, name, method)

    def clean(self):
        cleaned_data = super(BootstrapModelForm, self).clean()
        return cleaned_data


class CalendarWidget(forms.DateInput):
    def __init__(
        self,
        fecha_anterior=True,
        fecha_posterior=True,
        datetime=False,
        attrs={},
    ):
        clases = ""
        if datetime:
            clases = "datetimepicker"
        else:
            clases = "datepicker"

        attrs["autocomplete"] = "off"

        if fecha_anterior is False:
            clases = "%s validar_fecha_anterior" % clases

        if fecha_posterior is False:
            clases = "%s validar_fecha_posterior" % clases

        attrs["class"] = clases
        super(CalendarWidget, self).__init__(attrs=attrs)


class CalendarDateTimeWidget(forms.DateTimeInput):
    def __init__(
        self,
        fecha_anterior=True,
        fecha_posterior=True,
        datetime=False,
        attrs={},
    ):
        clases = ""
        if datetime:
            clases = "datetimepicker"
        else:
            clases = "datepicker"

        if fecha_anterior is False:
            clases = "%s validar_fecha_anterior" % clases

        if fecha_posterior is False:
            clases = "%s validar_fecha_posterior" % clases

        attrs["class"] = clases
        super(CalendarDateTimeWidget, self).__init__(attrs=attrs)


def validar_fecha(fecha):
    if fecha < datetime.datetime(1900, 1, 1).date():
        raise forms.ValidationError(u"El año no puede ser menor a 1900.")


def validar_fecha_anterior(fecha):
    if fecha < datetime.date.today():
        raise forms.ValidationError(
            u"No se puede seleccionar una fecha menor al día de hoy"
        )


def validar_fecha_posterior(fecha):
    if fecha > datetime.date.today():
        raise forms.ValidationError(
            u"No se puede seleccionar una fecha mayor al día de hoy"
        )


class CalendarDateField(forms.DateField):
    def __init__(
        self,
        fecha_anterior=True,
        fecha_posterior=True,
        attrs={},
        *args,
        **kwargs
    ):
        calendar_widget = CalendarWidget(
            fecha_anterior=fecha_anterior,
            fecha_posterior=fecha_posterior,
            datetime=False,
        )
        validators = [validar_fecha]
        if fecha_anterior is False:
            validators.append(validar_fecha_anterior)

        if fecha_posterior is False:
            validators.append(validar_fecha_posterior)

        super(CalendarDateField, self).__init__(
            input_formats=("%d/%m/%Y", "%Y-%m-%d"),
            widget=calendar_widget,
            validators=validators,
            *args,
            **kwargs
        )


class CalendarDateTimeField(forms.DateTimeField):
    def __init__(
        self,
        fecha_anterior=True,
        fecha_posterior=True,
        attrs={},
        *args,
        **kwargs
    ):
        calendar_widget = CalendarWidget(
            fecha_anterior=fecha_anterior,
            fecha_posterior=fecha_posterior,
            datetime=True,
        )

        if "widget" in kwargs:
            calendar_widget = kwargs["widget"]
            del kwargs["widget"]

        super(CalendarDateTimeField, self).__init__(
            input_formats=("%d/%m/%Y %H:%M", "%d/%m/%y %H:%M:%S"),
            widget=calendar_widget,
            *args,
            **kwargs
        )

class CustomImageWidget(ClearableFileInput):
    template_name = "utils/custom_file_input.html"