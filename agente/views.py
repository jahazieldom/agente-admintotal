from django.contrib import messages
from django.shortcuts import render, redirect, get_object_or_404
from .forms import (
    ConfiguracionInicialForm,
    ConfiguracionPinpadBanorteForm,
    ConfiguracionBiometricoForm,
    ConfiguracionBasculaForm,
    ConfiguracionGeneralForm,
)
from utils.forms import EMPTY_CHOICE
from .models import ConfiguracionGeneral, Tarea
from django.utils import timezone
from django.http import JsonResponse
from django.conf import settings
from utils.functions import list_view, tail_file, log
from django.urls import reverse
import os
import json
import requests
from bootstrap import StoppableThread, start_agent
from django.views.decorators.csrf import csrf_exempt
from scripts.installer import get_last_tag_version


AGENTE_THREAD = None

def get_almacenes(configuracion):
    try:
        url_admintotal = settings.ADMINTOTAL_URL.format(configuracion.clave)
        url = f"{url_admintotal}/api/v2/almacenes/"
        return requests.get(
            url, 
            headers={"Agente": configuracion.token, "Version-Agente": settings.VERSION}, 
            timeout=5,
        ).json().get("results")
    except Exception as e:
        return []

def get_configuracion():
    try:
        return ConfiguracionGeneral.objects.get()
    except Exception as e:
        return ConfiguracionGeneral.objects.create()


def eliminar_configuracion(request):
    conf = ConfiguracionGeneral.objects.first()
    conf.clave = None
    conf.token = None
    conf.fecha_configuracion = None
    conf.redis_url = None
    conf.save()
    return redirect("agente:inicio")


def configuracion_inicial(request):
    form = ConfiguracionInicialForm()
    if request.method == "POST":
        form = ConfiguracionInicialForm(request.POST)
        if form.is_valid():
            configuracion = get_configuracion()
            
            if not configuracion:
                configuracion = ConfiguracionGeneral()
            
            configuracion.clave = form.cleaned_data.get("clave")
            configuracion.token = form.cleaned_data.get("token")
            configuracion.redis_url = form.cleaned_data.get("redis_url")
            configuracion.nombre_agente = form.cleaned_data.get("nombre_agente")
            configuracion.razon_social = form.cleaned_data.get("razon_social")
            configuracion.rfc = form.cleaned_data.get("rfc")
            configuracion.fecha_configuracion = timezone.now()
            configuracion.save()
            
            thread_start_agent = StoppableThread(target=start_agent)
            thread_start_agent.start()
            return redirect("agente:inicio")

    return render(
        request,
        "agente/configuracion_inicial.html",
        {"form": form, "title": "Configuración inicial"},
    )


def inicio(request):
    from serial.tools import list_ports

    com_ports = list_ports.comports()
    form_general = ConfiguracionGeneralForm(instance=request.configuracion)
    form_bascula = ConfiguracionBasculaForm(instance=request.configuracion)
    form_banorte = ConfiguracionPinpadBanorteForm(instance=request.configuracion)
    form_biometrico = ConfiguracionBiometricoForm(instance=request.configuracion)
    
    form_banorte.fields["banorte_puerto_pinpad"].choices = EMPTY_CHOICE + [[x.device, x.description] for x in com_ports]
    form_bascula.fields["bascula_puerto"].choices = EMPTY_CHOICE + [[x.device, x.description] for x in com_ports]

    almacenes = []
    ultimas_tareas = Tarea.objects.filter(token=request.configuracion.token)[:10]
    form_submitted = None
    configuraciones_impresora_existente = os.path.exists(settings.PATH_CONFIGURACION_IMPRESION)

    if request.method == "POST":
        form_submitted = request.POST.get("form_submitted")

        if form_submitted == "general":
            form_general = ConfiguracionGeneralForm(
                request.POST, 
                request.FILES, 
                instance=request.configuracion
            )

            if form_general.is_valid():
                form_general.save()
                messages.success(request, "Los datos han sido guardados correctamente")
                configuraciones_impresora = form_general.cleaned_data.get("configuraciones_impresora")

                if configuraciones_impresora:                    
                    with open(settings.PATH_CONFIGURACION_IMPRESION, "wb") as output:
                        output.write(configuraciones_impresora.read())

                return redirect(
                    f"{request.path}?form_submitted={form_submitted}"
                )
        
        if form_submitted == "bascula":
            form_bascula = ConfiguracionBasculaForm(
                request.POST, instance=request.configuracion
            )
            form_bascula.fields["bascula_puerto"].choices = [[x.device, x.description] for x in com_ports]

            if form_bascula.is_valid():
                form_bascula.save()
                messages.success(request, "Los datos han sido guardados correctamente")
                return redirect(
                    f"{request.path}?form_submitted={form_submitted}"
                )
        if form_submitted == "biometrico":
            form_biometrico = ConfiguracionBiometricoForm(
                request.POST, instance=request.configuracion
            )

            if form_biometrico.is_valid():
                form_biometrico.save()
                messages.success(request, "Los datos han sido guardados correctamente")
                return redirect(
                    f"{request.path}?form_submitted={form_submitted}"
                )

        if form_submitted == "banorte":
            form_banorte = ConfiguracionPinpadBanorteForm(
                request.POST, instance=request.configuracion
            )
            form_banorte.fields["banorte_puerto_pinpad"].choices = [[x.device, x.description] for x in com_ports]
            if form_banorte.is_valid():
                form_banorte.save()
                messages.success(request, "Los datos han sido guardados correctamente")
                return redirect(
                    f"{request.path}?form_submitted={form_submitted}"
                )
        
        messages.error(request, "Corrija los errores para continuar")
    else:
        almacenes = get_almacenes(request.configuracion)

    return render(
        request,
        "agente/inicio.html",
        {
            "title": "Inicio",
            "form_biometrico": form_biometrico,
            "form_banorte": form_banorte,
            "form_bascula": form_bascula,
            "form_general": form_general,
            "ultimas_tareas": ultimas_tareas,
            "form_submitted": form_submitted,
            "almacenes": almacenes,
            "configuraciones_impresora_existente": configuraciones_impresora_existente,
        },
    )


def tareas(request):
    objects = Tarea.objects.filter(token=request.configuracion.token)
    return list_view(request, objects, "agente/tareas.html", {"title": "Tareas"})

def ver_tarea(request, id):
    tarea = get_object_or_404(Tarea, token=request.configuracion.token, id=id)
    return render(
        request,
        "agente/ver_tarea.html",
        {
            "title": f"{tarea.descripcion}",
            "tarea": tarea,
            "json_data": json.dumps(tarea.to_dict(ocultar_datos=True), indent=4),
            "transaccion_banorte": tarea.get_datos().get("codigo") in [1, 5]
        },
    )

def log_agente(request):
    from agente.functions import get_logs, get_log_file_path

    max_lines = int(request.GET.get("tail") or 100)
    log_str = get_logs(max_lines)
    
    if request.is_ajax():
        return JsonResponse({
            "log": log_str,
        })
    
    return render(
        request,
        "agente/logs.html",
        {
            "title": "Logs",
            "log": log_str,
            "log_file": get_log_file_path(),
        },
    )


def notificar_resultado_tarea(request, pk):
    tarea_instance = get_object_or_404(
        Tarea, pk=pk, resultado_notificado=False
    )
    tarea_instance.enviar_resultado_admintotal(
        configuracion=request.configuracion
    )
    return redirect("agente:inicio")


def banorte_cargar_llave_pinpad(request):
    from .functions import get_instance_banorte
    try:
        pinpad = get_instance_banorte()
        pinpad.cargar_llave()
        messages.success(request, "La llave ha sido cargada correctamente")
    except Exception as e:

        if "FileNotFoundException" in str(type(e)):
            messages.error(
                request, 
                f"No se tienen instalados los pre-requisitos del sistema para esta integración: <br>"
                f"<b>Visual C++ 2012 Redistributable (x86)</b> <br>"
                f"<b>Visual C++ 2013 Redistributable (x86)</b> <br><hr class='my-1'>"
                f"consulte la <b>Guía de configuración</b> para continuar."
            )
            return redirect("agente:inicio")
        else:
            messages.error(request, "Hubo un problema al cargar la llave, consulte los logs para más detalles.")

        log(str(e), "error")
        return redirect("agente:log_agente")

    return redirect("agente:inicio")

def preview_voucher(request, id):
    tarea = Tarea.objects.get(id=id)
    tipo_transaccion = "Venta"
    if tarea.get_datos().get("codigo") == 5:
        tipo_transaccion = "Cancelación"

    return render(request, "agente/pdf/voucher_banorte.html", {
        "tarea": tarea,
        "configuracion": request.configuracion, 
        "tipo_transaccion": tipo_transaccion, 
    })

def imprimir_voucher(request, id):
    from .functions import imprimir_voucher_banorte

    tarea = Tarea.objects.get(id=id)
    tipo_transaccion = "Venta"
    if tarea.get_datos().get("codigo") == 5:
        tipo_transaccion = "Cancelación"

    imprimir_voucher_banorte(tarea, tipo_transaccion=tipo_transaccion)
    return redirect(reverse("agente:ver_tarea", args=[id]))

@csrf_exempt
def agente_health_check(request):
    from .functions import health_check_agente

    resultado  = health_check_agente(request.configuracion)
    return JsonResponse(resultado)

def validar_ultima_version(request):
    ultima_version_str = get_last_tag_version()
    
    version_actual = sum([int(x) for x in settings.VERSION.split('.')])
    ultima_version = sum([int(x) for x in ultima_version_str.split('.')]) 

    return JsonResponse({
        "actualizacion_disponible": version_actual < ultima_version,
        "ultima_version": ultima_version_str, 
    })

def guia_actualizacion(request):
    return render(request, "agente/guia_actualizacion.html", {
        "title": "Guía de actualización",
        "configuracion": request.configuracion, 
        "ultima_version": get_last_tag_version(), 
        "path_dist": os.path.join(settings.BASE_DIR, "dist")
    })

@csrf_exempt
def abrir_explorador_archivos(request):
    import subprocess

    if request.method != "POST":
        return JsonResponse({"status": "error"})

    FILEBROWSER_PATH = os.path.join(os.getenv('WINDIR'), 'explorer.exe')
    path = request.POST.get("path")

    path = os.path.normpath(path)
    if os.path.isdir(path):
        subprocess.call([FILEBROWSER_PATH, path])
    elif os.path.isfile(path):
        subprocess.call([FILEBROWSER_PATH, '/select,', os.path.normpath(path)])
    
    return JsonResponse({"status": "success"})
    

@csrf_exempt
def detener_agente(request):
    import signal
    os.kill(os.getpid(), signal.SIGTERM)

def eliminar_configuracion_impresion(request):
    if os.path.exists(settings.PATH_CONFIGURACION_IMPRESION):
        os.remove(settings.PATH_CONFIGURACION_IMPRESION)
        messages.success(request, "La configuración ha sido eliminada correctamente.")
    return redirect("agente:inicio")

def guia_configuracion_banorte(request):
    return render(request, "agente/guia_configuracion_banorte.html", {
        "title": "Guía de configuración banorte",
    })

def nt_solicitud_lectura_lectura_huella(request):
    from agente.functions import nt_solicitud_lectura_lectura_huella
    nt_solicitud_lectura_lectura_huella()
    return redirect("/")
    
def iniciar_agente(request):
    thread_start_agent = StoppableThread(target=start_agent)
    thread_start_agent.start()
    messages.success(request, "El proceso se ha enviado a ejecución")
    return redirect("/")

def ajax_obtener_impresoras(request):
    from agente.functions import obtener_impresoras

    impresoras = []
    error = None

    try:
        impresoras = obtener_impresoras()
    except Exception as e:
        error = str(e)

    status = "success" if not error else "error"
    
    return JsonResponse({
        "status": status,
        "mensaje": error,
        "impresoras": impresoras
    })

def ajax_get_dispositivos_biometricos(request):
    from agente.functions import nt_get_dispositivos_choices

    data = {}
    
    try:
        dispositivos = nt_get_dispositivos_choices()
        data["status"] = "success"
        data["dispositivos"] = dispositivos
    except Exception as e:
        data["status"] = "error"
        data["mensaje"] = f"Error al obtener la lista de dispositivos: {e}"
    
    return JsonResponse(data)

def test_sentry(request):
    raise Exception("Test sentry")