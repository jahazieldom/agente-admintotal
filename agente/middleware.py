from agente.views import get_configuracion
from django.shortcuts import redirect
from django.urls import resolve

def check_config(get_response):

    def middleware(request):
        current_url = resolve(request.path_info).url_name
        configuracion = get_configuracion()

        if current_url != "configuracion_inicial" and not configuracion.token:
        	return redirect("agente:configuracion_inicial")

        request.configuracion = configuracion
        response = get_response(request)
        return response

    return middleware