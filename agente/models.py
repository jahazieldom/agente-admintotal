from django.db import models
from django.conf import settings
from django.utils.functional import cached_property
from utils.functions import localtime
import json

class ConfiguracionGeneral(models.Model):
    AMBIENTES_SANTANDER = (
        ("https://qa3.mitec.com.mx", "https://qa3.mitec.com.mx"),
        ("https://ssl.e-pago.com.mx", "https://ssl.e-pago.com.mx"),
        ("https://bc.mitec.com.mx", "https://bc.mitec.com.mx"),
        ("https://vip.e-pago.com.mx", "https://vip.e-pago.com.mx"),
        ("https://vip2.e-Pago.com.mx", "https://vip2.e-Pago.com.mx"),
    )

    MODELOS_PINPAD_BANORTE = (
        ("Vx810", "Vx810"),
        ("Vx820", "Vx820"),
    )

    MODELOS_PINPAD_SANTANDER = (
        ("Vx810", "Vx810"),
        ("Vx820", "Vx820"),
    )

    BAUD_RATES = (
        (50, "50"),
        (75, "75"),
        (110, "110"),
        (134, "134"),
        (150, "150"),
        (200, "200"),
        (300, "300"),
        (600, "600"),
        (1200, "1200"),
        (1800, "1800"),
        (2400, "2400"),
        (4800, "4800"),
        (9600, "9600"),
        (19200, "19200"),
        (38400, "38400"),
        (57600, "57600"),
        (115200, "115200"),
    )

    PARITIES = (
        ("PARITY_NONE", "PARITY_NONE"),
        ("PARITY_EVEN", "PARITY_EVEN"),
        ("PARITY_ODD", "PARITY_ODD"),
        ("PARITY_MARK", "PARITY_MARK"),
        ("PARITY_SPACE", "PARITY_SPACE"),
    )

    STOP_BITS = (
        ("STOPBITS_ONE", "STOPBITS_ONE"),
        ("STOPBITS_ONE_POINT_FIVE", "STOPBITS_ONE_POINT_FIVE"),
        ("STOPBITS_TWO", "STOPBITS_TWO"),
    )

    BYTE_SIZES = (
        ("FIVEBITS", "FIVEBITS"),
        ("SIXBITS", "SIXBITS"),
        ("SEVENBITS", "SEVENBITS"),
        ("EIGHTBITS", "EIGHTBITS"),
    )

    clave = models.CharField(max_length=255, null=True)
    token = models.CharField(max_length=500, db_index=True, null=True)
    nombre_agente = models.CharField(max_length=500, null=True)
    timezone = models.CharField(max_length=255, null=True)
    redis_url = models.CharField(max_length=2000, null=True)
    fecha_configuracion = models.DateTimeField(null=True)

    sentry_habilitado = models.BooleanField(default=True)

    rfc = models.CharField(max_length=255, null=True, blank=True, verbose_name="RFC")
    razon_social = models.CharField(max_length=255, null=True, blank=True, verbose_name="Razón social")
    direccion_calle = models.CharField(max_length=255, null=True, blank=True, verbose_name="Calle")
    direccion_numero_ext = models.CharField(max_length=255, null=True, blank=True, verbose_name="No. ext")
    direccion_numero_int = models.CharField(max_length=255, null=True, blank=True, verbose_name="No. int")
    direccion_colonia = models.CharField(max_length=255, null=True, blank=True, verbose_name="Colonia")
    direccion_cp = models.IntegerField(null=True, blank=True, verbose_name="C.P")
    telefono = models.CharField(max_length=255, null=True, blank=True, verbose_name="Teléfono")
    direccion_ciudad = models.CharField(max_length=255, null=True, blank=True, verbose_name="Ciudad")
    direccion_estado = models.CharField(max_length=255, null=True, blank=True, verbose_name="Estado")
    direccion_pais = models.CharField(max_length=255, null=True, blank=True, verbose_name="País")

    nombre_impresora = models.CharField(max_length=255, null=True, blank=True, verbose_name="Impresora")
    ancho_papel_impresora_px = models.IntegerField(default=335, verbose_name="Ancho del papel")

    banorte_contactless = models.BooleanField(default=False, null=True, verbose_name="¿Soporta tecnología contactless?")
    banorte_afiliacion = models.CharField(max_length=255, null=True, verbose_name="Afiliación")
    banorte_usuario = models.CharField(max_length=255, null=True, verbose_name="Usuario")
    banorte_password = models.CharField(max_length=255, null=True, verbose_name="Contraseña")
    banorte_puerto_pinpad = models.CharField(max_length=255, null=True, verbose_name="Puerto COM")

    santander_ambiente = models.CharField(max_length=255, choices=AMBIENTES_SANTANDER, null=True)
    santander_usuario = models.CharField(max_length=255, null=True)
    santander_password = models.CharField(max_length=255, null=True)

    bascula_torrey = models.BooleanField(default=False, verbose_name="¿Es báscula Torrey?")
    bascula_puerto = models.CharField(max_length=255, null=True, blank=True, verbose_name="Puerto serial")
    bascula_baudrate = models.PositiveIntegerField(choices=BAUD_RATES, default=9600, verbose_name="Baud rate")
    bascula_stopbits = models.CharField(max_length=50, choices=STOP_BITS, default="STOPBITS_ONE", verbose_name="Stop bits")
    bascula_parity = models.CharField(max_length=50, choices=PARITIES, default="PARITY_NONE", verbose_name="Parity")
    bascula_cmd = models.CharField(max_length=100, default="P", verbose_name="Comando")
    bascula_byte_size = models.CharField(max_length=100, choices=BYTE_SIZES, default="EIGHTBITS", verbose_name="Byte size")

    nt_license_trial = models.BooleanField(null=True, default=False, verbose_name="Licencia de prueba")
    nt_biometric_serial_number = models.CharField(max_length=255, null=True, verbose_name="Dispositivo biométrico")


    def get_direccion(self):
        direccion = ""

        if self.direccion_calle:
            direccion += f"{self.direccion_calle}"
        
        if self.direccion_numero_ext:
            direccion += f" {self.direccion_numero_ext}"
        
        if self.direccion_numero_int:
            direccion += f" Interior {self.direccion_numero_int}"
        
        if self.direccion_colonia:
            direccion += f" Col. {self.direccion_colonia},"
       
        if self.direccion_cp:
            direccion += f" CP {self.direccion_cp},"
        
        if self.direccion_ciudad:
            direccion += f" {self.direccion_ciudad},"
        
        if self.direccion_estado:
            direccion += f" {self.direccion_estado}"

        return direccion

    def get_url_admintotal(self):
        return settings.ADMINTOTAL_URL.format(self.clave)

    @property
    def biometrico_habilitado(self):
        return bool(
            self.nt_biometric_serial_number
        )
    
    @property
    def bascula_habilitado(self):
        return bool(
            self.bascula_puerto and 
            self.bascula_baudrate
        )

    @property
    def banorte_habilitado(self):
        return bool(
            self.banorte_afiliacion and 
            self.banorte_usuario and 
            self.banorte_password and 
            self.banorte_puerto_pinpad 
        )

class Tarea(models.Model):
    token = models.CharField(max_length=500, db_index=True, null=True)
    inicio = models.DateTimeField(null=True)
    fin = models.DateTimeField(null=True)
    admintotal_id = models.PositiveIntegerField()
    descripcion = models.TextField(null=True)
    # __TODO__: cambiar a JSONField, no puso desde un inicio por que el plan
    # es usar sqlite de entrada.
    resultado = models.TextField(null=True)
    datos = models.TextField(null=True)
    creado = models.DateTimeField(auto_now_add=True)
    # será True cuando se envíe el resultado a admintotal y admintotal
    # responda con status code 200
    resultado_notificado = models.BooleanField(default=False)
    
    class Meta:
        ordering = ("-creado",)

    def mostrar_autorizado_con_firma_electronica(self):
        from utils.functions import to_decimal
        
        resultado_banorte = self.resultado_banorte or {}
        
        if not resultado_banorte:
            return False
        
        if resultado_banorte.get("captura_nip"):
            return True
        
        if resultado_banorte.get("modo_entrada") == "CONTACTLESSCHIP":
            monto = to_decimal(self.resultado_banorte.get("monto"))
            if monto <= 400:
                return True
            
        return False

    @staticmethod
    def from_dict(tarea_dict, token=None):
        """
        Crea una instancia basado en el diccionario recibido de admintotal.com
        """
        admintotal_id = tarea_dict.get("id")
        descripcion = tarea_dict.get("codigo_display")
        datos = tarea_dict.get("datos")

        return Tarea.objects.create(
            token=token,
            admintotal_id=admintotal_id,
            descripcion=descripcion,
            datos=json.dumps(datos),
        )

    def to_dict(self, ocultar_datos=False):
        resultado = self.get_resultado(ocultar_datos=ocultar_datos)
        datos = self.get_datos()
        resultado["inicio"] = localtime(self.inicio).isoformat()
        resultado["fin"] = localtime(self.fin).isoformat()
        return {
            "admintotal_id": self.admintotal_id,
            "inicio": self.inicio.isoformat(),
            "fin": self.fin.isoformat(),
            "resultado": resultado,
            "datos": datos,
        }

    def get_descripcion_detallada(self):
        resultado = []
        for k, v in self.get_datos().items():
            resultado.append(f"{k}: {v}")

        return ", ".join(resultado)

    def get_resultado(self, ocultar_datos=False):
        try:
            resultado = json.loads(self.resultado) or {}
            if ocultar_datos:
                if "base64_template" in resultado:
                    resultado["base64_template"] = "**** contenido oculto ****"

                if "base64_image" in resultado:
                    resultado["base64_image"] = "**** contenido oculto ****"

            return resultado
        except Exception as e:
            return {}

    def get_datos(self):
        try:
            return json.loads(self.datos)
        except Exception as e:
            return {}

    def enviar_resultado_admintotal(self, configuracion=None):
        from .functions import notificar_resultado
        if not self.fin:
            return False

        return notificar_resultado(self, configuracion=configuracion)

    def notificar_resultado_http(self):
        datos = self.get_datos()
        return datos.get("sync", True) == False
    
    @cached_property
    def resultado_banorte(self):
        return self.get_resultado().get("data", {})
    
    @cached_property
    def es_transaccion_pinpad(self):
        return self.get_datos().get("codigo") in [5, 1]
