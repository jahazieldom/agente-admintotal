from agente.models import ConfiguracionGeneral
from .views import get_configuracion
from django.conf import settings
from django.utils import timezone
import requests
import time
import json
import tempfile
import subprocess
import os
import serial
from contextlib import contextmanager
from utils.functions import to_decimal, log, render_pdf
import random


def remove_control_characters(s):
    return ''.join(e for e in s if e.isalnum() or e == '.')

def peso_valido_magellan(peso):
    if not peso:
        return False

    if "A" in peso or "E" in peso:
        return False

    return bool(to_decimal(peso))


def validar_token_admintotal(*, clave, token):
    url_admintotal = settings.ADMINTOTAL_URL.format(clave)
    url = "{}/agentes/_configurar/".format(url_admintotal, token)
    try:
        response = requests.post(url, json={"token": token}, headers={"Version-Agente": settings.VERSION})
        if response.ok:
            return response.json()
        raise Exception(f"Admintotal respondió con status {response.status_code}")
    except Exception as e:
        error = str(e)
        return {
            "status": "error",
            "mensaje": f"Hubo un error al validar la configuración: {error}"
        }

def consultar_tareas(configuracion=None):
    if not configuracion:
        configuracion = get_configuracion()

    if not configuracion:
        raise Exception("No se encontró ConfiguracionGeneral")

    url = "{}/agentes/_consultar_tareas/{}/".format(
        configuracion.get_url_admintotal(), 
        configuracion.token
    )
    tareas = []
    try:
        data = requests.get(url, headers={"Version-Agente": settings.VERSION}).json()
        if data.get("status") == "success":
            tareas = data.get("tareas")
    except Exception as e:
        pass

    return tareas

def get_instance_banorte(conf=None):
    from lib.banorte.pinpad import PinpadBanorte
    
    if not conf:
        conf = ConfiguracionGeneral.objects.first()

    instancia = PinpadBanorte(
        afiliacion=conf.banorte_afiliacion, 
        usuario=conf.banorte_usuario, 
        password=conf.banorte_password, 
        puerto=conf.banorte_puerto_pinpad, 
        contactless=conf.banorte_contactless
    )

    recargar_instancia = (
        instancia.afiliacion != conf.banorte_afiliacion or
        instancia.usuario != conf.banorte_usuario or
        instancia.password != conf.banorte_password or
        instancia.puerto != conf.banorte_puerto_pinpad or
        instancia.contactless != conf.banorte_contactless
    )    

    return PinpadBanorte(
        afiliacion=conf.banorte_afiliacion, 
        usuario=conf.banorte_usuario, 
        password=conf.banorte_password, 
        puerto=conf.banorte_puerto_pinpad, 
        contactless=conf.banorte_contactless, 
        recargar_instancia=recargar_instancia,
    )

def banorte_cancelar_transaccion(*, referencia, monto, configuracion=None):
    instance = get_instance_banorte(conf=configuracion)
    return instance.cancelar_transaccion(referencia, monto)

def banorte_cobro_tarjeta(*, monto, referencia, configuracion=None):
    if not es_windows():
        import time

        time.sleep(8)
        return {
            "consulta_verificacion": False,
            "data": {
                "afiliacion": "9999",
                "aid": "A0000000032010",
                "al": "VISA ELECTRON",
                "apn": "BANCOMER VISA",
                "autorizacion": "9999",
                "banco_emisor": "BANCOMER",
                "captura_nip": True,
                "declinado_chip": False,
                "declinado_emv": False,
                "emv_tags": "",
                "expiracion_tarjeta": "09/26",
                "fecha": timezone.now().strftime("%d/%m/%Y %H:%M"),
                # "fecha_respuesta": "Wed, 12 Jul 2023 22:52:26 GMT",
                "fecha_respuesta": timezone.now().strftime("%a, %d %b %Y %H:%M:%S %Z"),
                "marca_tarjeta": "VISA------",
                "mensaje": "Approved",
                "monto": str(monto),
                "numero_control": referencia,
                "numero_tarjeta": "************9999",
                "referencia": referencia,
                "resultado": "A",
                "tarjetahabiente": "                          ",
                "terminal": "804739764",
                "tipo_tarjeta": "DEBITO",
                "tsi": "6800",
                "tvr": "8080008000"
            },
            "imprimir_comprobante": True,
            "mensaje": "Approved",
            "status": "success",
        }
    
    instance = get_instance_banorte(conf=configuracion)
    return instance.realizar_cobro(monto, referencia)

def cobrar_pinpad_santander(*, monto, cajero, referencia, configuracion):
    from lib.santander.pinpad import PinpadSantander
    
    instance = PinpadSantander.get_instance(configuracion)
    return instance.realizar_cobro(monto, cajero, referencia)

def notificar_resultado(tarea_instance, configuracion=None):
    if not configuracion:
        configuracion = get_configuracion()

    url = "{}/agentes/_finalizar_tarea/{}/{}/".format(
        configuracion.get_url_admintotal(), 
        configuracion.token,
        tarea_instance.admintotal_id,
    )

    response = requests.post(url, json=tarea_instance.to_dict(), headers={"Version-Agente": settings.VERSION})
    if response.ok:
        tarea_instance.resultado_notificado = True
        tarea_instance.save()
        
    return tarea_instance.resultado_notificado

def obtener_peso_bascula(info, configuracion=None):
    inicio = timezone.now().isoformat()
    log("Iniciando comunicación con puerto serial...", level="debug")
    
    if configuracion is None:
        configuracion = get_configuracion()
    
    if configuracion.bascula_torrey:
        return obtener_peso_bascula_torrey(info, configuracion=configuracion)

    puerto = configuracion.bascula_puerto
    baudrate = configuracion.bascula_baudrate

    if settings.LOCAL_DEVELOPMENT:
        return {
            "inicio": timezone.now().isoformat(),
            "fin": timezone.now().isoformat(),
            "resultado": str(round(random.uniform(1, 49.99), 2)),
            "meta": {
                "puerto": puerto,
                "baudrate": baudrate,
                "paridad": configuracion.bascula_parity,
                "stopbits": configuracion.bascula_stopbits,
            }
        }
    
    try:
        ser = serial.Serial(
            puerto, 
            baudrate=baudrate, 
            parity=getattr(serial, configuracion.bascula_parity, None), 
            stopbits=getattr(serial, configuracion.bascula_stopbits, None), 
            bytesize=getattr(serial, configuracion.bascula_byte_size, None), 
            timeout=1
        )
    except Exception as e:
        return {
            "status": "error",
            "mensaje": f"Error con el puerto serial: \n{e}",
            "resultado": None
        }
    
    log("Solicitando peso...", level="debug")
    comando_peso = b"\x57\x1E"
    ser.write(comando_peso)
    peso = remove_control_characters(ser.readline().decode("utf-8"))
    log("Procesando respuesta...", level="debug")

    max_intentos = 5
    intentos = 0
    
    while True:
        intentos += 1

        if "A" in peso or "E" in peso:
            log(f"Peso: {peso}", level="debug")
            ser.write(comando_peso)
            peso = remove_control_characters(ser.readline().decode("utf-8"))
            time.sleep(.1)

        if intentos == max_intentos or bool(to_decimal(peso)):
            # si ya se llegó al límite de intentos o 
            # si ya tenemos un valor de peso
            break

    # ser.flush()
    ser.close()
    fin = timezone.now().isoformat()

    return {
        "inicio": inicio,
        "fin": fin,
        "resultado": str(to_decimal(peso)),
        "meta": {
            "puerto": puerto,
            "baudrate": baudrate,
            "paridad": configuracion.bascula_parity,
            "stopbits": configuracion.bascula_stopbits,
        }
    }

def obtener_peso_bascula_torrey(info, configuracion=None):
    inicio = timezone.now().isoformat()
    log("Iniciando comunicación con puerto serial...", level="debug")
    
    if configuracion is None:
        configuracion = get_configuracion()

    puerto = configuracion.bascula_puerto
    baudrate = configuracion.bascula_baudrate

    if settings.LOCAL_DEVELOPMENT:
        return {
            "inicio": timezone.now().isoformat(),
            "fin": timezone.now().isoformat(),
            "resultado": str(round(random.uniform(1, 49.99), 2)),
            "meta": {
                "puerto": puerto,
                "baudrate": baudrate,
                "paridad": configuracion.bascula_parity,
                "stopbits": configuracion.bascula_stopbits,
            }
        }
    
    try:
        ser = serial.Serial(
            puerto, 
            baudrate=baudrate, 
            parity=getattr(serial, configuracion.bascula_parity, None), 
            stopbits=getattr(serial, configuracion.bascula_stopbits, None), 
            bytesize=getattr(serial, configuracion.bascula_byte_size, None), 
            timeout=1
        )
    except Exception as e:
        return {
            "status": "error",
            "mensaje": f"Error con el puerto serial: \n{e}",
            "resultado": None
        }
    
    log("Solicitando peso...", level="debug")
    comando_peso = bytes(configuracion.bascula_cmd.encode())
    ser.write(comando_peso)
    peso = remove_control_characters(ser.readline().decode("utf-8"))
    log("Procesando respuesta...", level="debug")

    max_intentos = 5
    intentos = 0
    
    while True:
        intentos += 1
        peso = (peso or "").lower()
        if "kg" in peso:
            peso = peso.replace("kg", "")
        else:
            log(f"Peso: {peso}", level="debug")
            ser.write(comando_peso)
            peso = remove_control_characters(ser.readline().decode("utf-8"))
            time.sleep(.1)

        if intentos == max_intentos or bool(to_decimal(peso)):
            # si ya se llegó al límite de intentos o 
            # si ya tenemos un valor de peso
            break

    # ser.flush()
    ser.close()
    fin = timezone.now().isoformat()

    return {
        "inicio": inicio,
        "fin": fin,
        "resultado": str(to_decimal(peso)),
        "meta": {
            "puerto": puerto,
            "baudrate": baudrate,
            "paridad": configuracion.bascula_parity,
            "stopbits": configuracion.bascula_stopbits,
        }
    }

def imprimir_voucher_banorte(tarea, tipo_transaccion="Venta", configuracion=None):
    if not configuracion:
        configuracion = get_configuracion()
    
    referencia = tarea.resultado_banorte.get("referencia")
    pdf_tmp_file = os.path.join(tempfile.gettempdir(), f"voucher-{referencia}.pdf")

    render_pdf("agente/pdf/voucher_banorte.html", "voucher", {
        'tarea': tarea,
        "tipo_transaccion": tipo_transaccion, 
        "configuracion": configuracion, 
    }, to_file=pdf_tmp_file, opciones={"margins": False})

    imprimir_pdf(pdf_tmp_file, configuracion=configuracion)

def imprimir_url(url, configuracion=None, nombre_impresora=None): 
    file_name = f'imprimir-{str(time.time()).replace(".", "-")}.pdf'
    pdf_tmp_file = os.path.join(tempfile.gettempdir(), file_name)
    pdf_response = requests.get(url, timeout=(1, 10))

    with open(pdf_tmp_file, "wb") as pdf_file:
        pdf_file.write(pdf_response.content)
    
    return imprimir_pdf(pdf_tmp_file, configuracion=configuracion, nombre_impresora=nombre_impresora)


def imprimir_pdf(pdf_file, configuracion=None, nombre_impresora=None):
    if not es_windows():
        print("Impresión de pdf solo disponible en windows")
        return

    if not configuracion:
        configuracion = get_configuracion()

    if not nombre_impresora:
        nombre_impresora = configuracion.nombre_impresora
        
    exe_tool = os.path.join(settings.BASE_DIR, "lib", "PDFtoPrinter.exe")
    command = [exe_tool, pdf_file]

    if nombre_impresora:
        command.append(nombre_impresora)

    log("Comando de impresión:")
    log(" ".join(command))
    subprocess.call(command)

def health_check_agente(configuracion):
    import redis

    tarea_id = "hc"
    timeout = 5
    canal = None
    
    if not configuracion.redis_url:
        return {
            "status": "error",
            "mensaje": "No hay un token de conexión en la configuración"
        }

    try:
        r = redis.Redis.from_url(
            configuracion.redis_url, 
            socket_timeout=timeout, 
        )
        r.publish(configuracion.token, json.dumps({"id": tarea_id, "codigo": "100"}))
        
        # esperamos la respuesta en el canal especifico
        # no modificar, en caso de modificar también será necesario
        # modificar el agente!!!
        pubsub = r.pubsub(ignore_subscribe_messages=True)
        canal = f"{configuracion.clave}_{configuracion.token}_{tarea_id}"
        pubsub.subscribe(canal)
        resultado = {}

        for message in pubsub.listen():
            try:
                resultado = json.loads(message["data"]).get("resultado")
                pubsub.close()
                break
            except Exception as e:
                log("Error al parsear json", "error")
                resultado = {
                    "status": "error",
                    "mensaje": "Error al parsear json de la respuesta"
                }
                break

    except redis.exceptions.ConnectionError as e:
        resultado = {
            "status": "error",
            "mensaje": (
                f"No se pudo establecer conexión con el servidor de Redis"
            )
        }
    
    except redis.exceptions.TimeoutError as e:
        resultado = {
            "status": "error",
            "mensaje": (
                f"No se recibió respuesta por parte del "
                f"agente en {timeout} segundos"
            )
        }
    
    resultado["canal"] = canal
    return resultado

def get_nt_fingerprint_client():
    import sys
    
    es_64bits = sys.maxsize > 2**32

    if not es_64bits:
        raise Exception("Es necesaria la instalación de 64 Bits del Agente")

    from lib.neurotechnology.fingerprint import FingerPrintNeurotec

    configuracion = get_configuracion()
    """
    if not configuracion.biometrico_habilitado:
        raise Exception(f"No se encuentra habilitada la integración con biométrico")
    """
    return FingerPrintNeurotec(
        biometric_serial_number=configuracion.nt_biometric_serial_number,
        trial=bool(configuracion.nt_license_trial),
    )

def nt_get_dispositivos_choices():
    fp = get_nt_fingerprint_client()
    return fp.get_devices(as_tuple=True)

def nt_solicitud_lectura_lectura_huella():
    inicio = timezone.now().isoformat()
    fp = get_nt_fingerprint_client()
    res = fp.finger_scan()
    fin = timezone.now().isoformat()

    if res.get("base64_image"):
        return {
            "status": "success",
            "inicio": inicio,
            "fin": fin,
            "base64_image": res.get("base64_image"),
            "base64_template": res.get("base64_template"),
        }

    return {
        "status": "error",
        "inicio": inicio,
        "fin": fin,
        "error": res.get("error"),
        "mensaje": res.get("mensaje"),
    }

def nt_cancelar_lectura_lectura_huella():
    inicio = timezone.now().isoformat()
    fp = get_nt_fingerprint_client()
    res = fp.cancelar_operacion()
    fin = timezone.now().isoformat()
    return {
        "status": "success",
        "inicio": inicio,
        "fin": fin,
    }   

def obtener_impresoras():
    import wmi
    import pythoncom

    pythoncom.CoInitialize()

    impresoras = []
    # Conectar al Namespace de WMI para obtener información del sistema
    conexion_wmi = wmi.WMI()
    
    # Obtener la lista de impresoras instaladas
    consulta = "SELECT * FROM Win32_Printer"
    resultado = conexion_wmi.query(consulta)
    
    # Recorrer los resultados y obtener el nombre de cada impresora
    for impresora in resultado:
        nombre_impresora = impresora.Name
        impresoras.append(nombre_impresora)
    
    return impresoras

def es_windows():
    import platform

    return platform.system() == "Windows"

@contextmanager
def catch_stdout(buff):
    """DOCS"""
    import sys

    stdout = sys.stdout
    sys.stdout = buff
    yield
    sys.stdout = stdout

def ejecutar_comando(cmd):
    import traceback
    from io import StringIO

    buff = StringIO()
    error = False
    try:
        with catch_stdout(buff):
            exec(cmd, {}, {
                "_": print,
                "configuracion": ConfiguracionGeneral.objects.first(),
                "settings": settings,
                "get_log_file_path": get_log_file_path,
                "get_logs": get_logs,
            })
    except:
        traceback.print_exc(file=buff)
        error = True

    salida = buff.getvalue()
    return salida, error

def get_log_file_path():
    return os.path.join(settings.LOG_DIR, "agente.log")

def get_logs(max_lines):
    from utils.functions import tail_file
    
    log_file = get_log_file_path()
    with open(log_file, "rb") as f:
        log_str = tail_file(f, max_lines).decode('cp1252')
    return log_str