from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from . import views

app_name = "agente"

app_patterns = (
    [
        path("", views.inicio, name="inicio"),
        path("_iniciar_agente/", views.iniciar_agente, name="iniciar_agente"),
        path("_health_check/", views.agente_health_check, name="agente_health_check"),
        path("tareas/", views.tareas, name="tareas"),
        path("ver_tarea/<int:id>/", views.ver_tarea, name="ver_tarea"),
        path("imprimir_voucher/<int:id>/", views.imprimir_voucher, name="imprimir_voucher"),
        path("preview_voucher/<int:id>/", views.preview_voucher, name="preview_voucher"),
        path("log/agente/", views.log_agente, name="log_agente"),
        path(
            "configuracion_inicial/",
            views.configuracion_inicial,
            name="configuracion_inicial",
        ),
        path(
            "eliminar_configuracion", 
            views.eliminar_configuracion, 
            name="eliminar_configuracion"
        ),
        path(
            "notificar_resultado_tarea/<int:pk>/", 
            views.notificar_resultado_tarea, 
            name="notificar_resultado_tarea"
        ),

        path(
            "banorte_cargar_llave_pinpad/", 
            views.banorte_cargar_llave_pinpad, 
            name="banorte_cargar_llave_pinpad"
        ),

        path(
            "validar_ultima_version/", 
            views.validar_ultima_version, 
            name="validar_ultima_version"
        ),

        path(
            "guia_actualizacion/", 
            views.guia_actualizacion, 
            name="guia_actualizacion"
        ),
        
        path(
            "abrir_explorador_archivos/", 
            views.abrir_explorador_archivos, 
            name="abrir_explorador_archivos"
        ),
        path(
            "detener_agente/", 
            views.detener_agente, 
            name="detener_agente"
        ),
        path(
            "eliminar_configuracion_impresion/", 
            views.eliminar_configuracion_impresion, 
            name="eliminar_configuracion_impresion"
        ),
        path(
            "guia_configuracion_banorte/", 
            views.guia_configuracion_banorte, 
            name="guia_configuracion_banorte"
        ),
        path(
            "nt_solicitud_lectura_lectura_huella/", 
            views.nt_solicitud_lectura_lectura_huella, 
            name="nt_solicitud_lectura_lectura_huella"
        ),
        path(
            "ajax/obtener_impresoras/", 
            views.ajax_obtener_impresoras, 
            name="ajax_obtener_impresoras"
        ),
        path(
            "ajax/get_dispositivos_biometricos/", 
            views.ajax_get_dispositivos_biometricos, 
            name="ajax_get_dispositivos_biometricos"
        ),
        path(
            "test_sentry/", 
            views.test_sentry, 
            name="test_sentry"
        ),
    ],
    "agente",
)

urlpatterns = [
    path("", include(app_patterns)),
    path("sh/", include("sh.urls")),
    path('su/', admin.site.urls),
] 
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) 
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

