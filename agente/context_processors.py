from django.conf import settings

def custom_context(request):
    return {
        "SETTINGS": settings
    }