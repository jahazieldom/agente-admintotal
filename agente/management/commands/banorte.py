from django.core.management.base import BaseCommand
from django.utils import timezone
from agente.settings import BASE_DIR
import os
from utils.functions import log, localtime
import clr
import sys
from System import Reflection



class Command(BaseCommand):

    def handle(self, *args, **kwargs):
        banorte_base_dir = os.path.join(BASE_DIR, "lib", "banorte")

        dll_path = os.path.join(banorte_base_dir, "BanortePinPadSeguro.dll")
        print(f"Agregando referencia: {dll_path}")
        
        print(f"*" * 15)

        try:
            #Reflection.Assembly.LoadFile(dll_path)
            clr.AddReference(dll_path)

            from System.Collections import Hashtable
            import Banorte
            
        except Exception as e:
            self.stderr.write(str(e))

        
