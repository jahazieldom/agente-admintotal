from django.core.management.base import BaseCommand
from django.utils import timezone
from agente.functions import (
    obtener_peso_bascula,
    banorte_cobro_tarjeta,
    banorte_cancelar_transaccion,
    imprimir_voucher_banorte,
    health_check_agente,
    imprimir_url,
    ejecutar_comando,
)
from agente.models import Tarea
from agente.views import get_configuracion, inicio
from django.conf import settings
from utils.functions import localtime, log, check_pid
import time
import os
import json
import redis
import threading

class Command(BaseCommand):
    help = "Agente CLI"       

    def _get_pid_path(self):
        """
        Retorna el path al archivo pid del agente.
        """
        return os.path.join(settings.BASE_DIR, "agent_daemon.pid")
    
    def _get_nombre_canal(self, configuracion, tarea_id):
        return f"{configuracion.clave}_{configuracion.token}_{tarea_id}"

    def notificar_resultado(self, redis_url, canal, resultado, write_log=True, tarea_obj=None):
        """
        Envía el mensaje con el resultado de la tarea ejecutada por el agente.
        """

        if tarea_obj:
            if tarea_obj.notificar_resultado_http():
                log(f"Resultado enviado via HTTP")
                return tarea_obj.enviar_resultado_admintotal()

        try:
            r = redis.Redis.from_url(redis_url)
            r.publish(
                canal,
                json.dumps({"evento": "finalizado", "resultado": resultado}),
            )

            if write_log:
                log(f"Resultado enviado al canal: {canal}")
            
            return True
        except Exception as e:
            log("Error al notificar el comando:", "error")
            log(str(e), "error")

        return False

    def procesar_comando(self, json_info, configuracion):
        """
        Método encargado de procesar el comando dependiendo el tipo de tarea,
        TAREAS_AGENTE = (
            (1, "Cobro con tarjeta"),
            (2, "Reimprimir voucher"),
            (3, "Consultar transacción"),
            (4, "Leer báscula"),
            (5, "Cancelar transacción pinpad"),
            (6, "Imprimir URL"),
            (7, "Leer huella"),
            (8, "Cancelar lectura de huella"),
            (100, "Health check"),
        ) 
        """
        
        if not json_info:
            return 
        
        comando = str(json_info.get("codigo", ""))
        tarea_id = json_info.get("id", "")
        canal = self._get_nombre_canal(configuracion, tarea_id)
        notificado = False

        resultado = {
            "status": "error",
            "mensaje": f"El comando {comando} no fué procesado",
        }

        if comando == "100":
            # Check agente
            time.sleep(.5)
            return self.notificar_resultado(
                redis_url=configuracion.redis_url,
                canal=canal,
                resultado={"status": "success"},
                write_log=False
            )
        
        if comando == "4":
            # Obtener peso de la báscula
            resultado = obtener_peso_bascula(
                json_info, configuracion=configuracion
            )
            return self.notificar_resultado(
                redis_url=configuracion.redis_url,
                canal=canal,
                resultado=resultado,
            )

        tarea_obj = Tarea(
            token=configuracion.token,
            inicio=localtime(),
            admintotal_id=json_info.get("id", ""),
            datos=json.dumps(json_info),
            descripcion=json_info.get("codigo_display", "")
        )

        try:
            if comando == "1":
                resultado = banorte_cobro_tarjeta(
                    monto=json_info["datos"]["monto"], 
                    referencia=json_info["datos"]["referencia"],
                    # configuracion=configuracion,
                )

            if comando == "5":
                resultado = banorte_cancelar_transaccion(
                    referencia=json_info["datos"]["referencia"],
                    monto=json_info["datos"]["monto"],
                    # configuracion=configuracion,
                )

            if comando == "6":
                nombre_impresora = json_info["datos"].get("nombre_impresora")
                imprimir_url(json_info["datos"]["url"], nombre_impresora=nombre_impresora)
                resultado = {
                    "status": "success",
                    "mensaje": f"El comando {comando} fué procesado",
                }

            if comando in ["7", "8"]:
                # comandos para comunicacion con biometrico
                from agente.functions import (
                    nt_solicitud_lectura_lectura_huella, 
                    nt_cancelar_lectura_lectura_huella,
                )
                
                if comando == "7":
                    resultado = nt_solicitud_lectura_lectura_huella()

                if comando == "8":
                    resultado = nt_cancelar_lectura_lectura_huella()

            if comando == "9":
                cmd = json_info["datos"].get("cmd")
                output, error = ejecutar_comando(cmd)
                mensaje = "El comando ha sido procesado"
                if error:
                    mensaje = error
                resultado = {
                    "status": "success" if not error else "error",
                    "mensaje": mensaje,
                    "output": output,

                }
                                
        except Exception as e:
            resultado = {
                "status": "error",
                "mensaje": f"Error al procesar el comando: {e}"
            }

        tarea_obj.resultado = json.dumps(resultado)
        tarea_obj.fin = localtime()

        notificado = self.notificar_resultado(
            redis_url=configuracion.redis_url,
            canal=canal,
            resultado=resultado,
            tarea_obj=tarea_obj,
        )
        
        tarea_obj.resultado_notificado = notificado
        tarea_obj.save()

        if resultado and resultado.get("imprimir_comprobante") and comando == "1":
            imprimir_voucher_banorte(tarea_obj)
            
            if resultado.get("status") == "success":
                imprimir_voucher_banorte(tarea_obj)

        if resultado and resultado.get("imprimir_comprobante") and comando == "5":
            imprimir_voucher_banorte(tarea_obj, tipo_transaccion="Cancelación")

        return tarea_obj

    def check_daemon_status(self):
        """
        Método encargado de revisar si ya hay un agente en ejecución
        """
        pid_path = self._get_pid_path()
        if os.path.exists(pid_path):
            return log("Agente en ejecución")
        return log("El agente no se encuentra en ejecución", "error")

    def pubsub_stream(self, redis_url, token):
        """
        Método que crea la subscripción al canal del token del agente y 
        recibir las tareas a ejecutar.
        """
        try:
            r = redis.Redis.from_url(
                redis_url, 
                socket_timeout=30, 
                retry_on_timeout=True,
                socket_keepalive=True,
            )
            pubsub = r.pubsub(ignore_subscribe_messages=True)
            pubsub.subscribe(token)
            log(f"Conectado al canal: {token}")

            habilitar_thread = False

            for message in pubsub.listen():
                configuracion = get_configuracion()

                try:
                    json_data = json.loads(message["data"])
                    if json_data.get("codigo") != "100":
                        log(f"Nuevo mensaje recibido")
                        log(message["data"])
                except Exception as e:
                    log("Error al ejecutar json.loads", "error")
                    log(message["data"])
                    log(str(e), "error")
                    continue
                    
                try:
                    if habilitar_thread:
                        thread = threading.Thread(
                            target=self.procesar_comando, 
                            args=[json_data], 
                            kwargs={"configuracion": configuracion}
                        )
                        thread.start()
                    else:
                        self.procesar_comando(json_data, configuracion=configuracion)
                except Exception as e:
                    log("Error al procesar el comando:", "error")
                    log(str(e), "error")

        except redis.exceptions.ConnectionError as e:
            log(str(e), "error")
            time.sleep(3)
            log(f"Intentando nueva conexión...")
            self.pubsub_stream(redis_url, token)
    

    def start_daemon(self):
        configuracion = get_configuracion()
        agente_status = health_check_agente(configuracion)
        
        if agente_status.get("status") == "success":
            return log("Ya se encuentra corriendo un agente.", "error")

        if not configuracion or not configuracion.token:
            return log("No se ha configurado el agente.", "error")

        if not configuracion.redis_url:
            return log(
                "No se tiene configurada la conexión con el servidor de Redis.",
                "error",
            )

        self.pubsub_stream(
            redis_url=configuracion.redis_url, 
            token=configuracion.token,
        )

    def add_arguments(self, parser):
        parser.add_argument("accion", type=str, help="iniciar|status")

    def handle(self, *args, **kwargs):
        accion = kwargs["accion"]

        if accion == "iniciar":
            return self.start_daemon()

        elif accion == "status":
            return self.check_daemon_status()
        
        else:
            log("La acción %s no es válida." % accion, "error")
