from django.core.management.base import BaseCommand
from django.utils import timezone
from agente.functions import (
    get_nt_fingerprint_client,
    log,
)


class Command(BaseCommand):
    help = "Pruebas de integración con neurotechnology (biometricos)"       

    def add_arguments(self, parser):
        parser.add_argument("-m", "--method", type=str, help="Nombre del método a ejecutar ")

    def handle(self, *args, **kwargs):
        instance_method = kwargs["method"]
        client = get_nt_fingerprint_client()
        if instance_method and hasattr(client, instance_method):
            log(f"Ejecutando {instance_method} de la instancia {client.__class__}")
            log(getattr(client, instance_method)())
        
