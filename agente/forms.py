from .models import ConfiguracionGeneral
from django import forms
from utils.forms import BootstrapForm, BootstrapModelForm
from utils.functions import log

def get_choices_impresoras():
    import os

    if os.name != 'nt':
        # si no es windows retornamos lista vacia
        # TODO: implementar método para detectar impresoras 
        return []
    try:
        import ctypes
        from ctypes.wintypes import BYTE, DWORD, LPCWSTR

        winspool = ctypes.WinDLL('winspool.drv')  # for EnumPrintersW
        msvcrt = ctypes.cdll.msvcrt  # for malloc, free

        # Parameters: modify as you need. See MSDN for detail.
        PRINTER_ENUM_LOCAL = 2
        Name = None  # ignored for PRINTER_ENUM_LOCAL
        Level = 1  # or 2, 4, 5

        class PRINTER_INFO_1(ctypes.Structure):
            _fields_ = [
                ("Flags", DWORD),
                ("pDescription", LPCWSTR),
                ("pName", LPCWSTR),
                ("pComment", LPCWSTR),
            ]

        # Invoke once with a NULL pointer to get buffer size.
        info = ctypes.POINTER(BYTE)()
        pcbNeeded = DWORD(0)
        pcReturned = DWORD(0)  # the number of PRINTER_INFO_1 structures retrieved
        winspool.EnumPrintersW(PRINTER_ENUM_LOCAL, Name, Level, ctypes.byref(info), 0,
                ctypes.byref(pcbNeeded), ctypes.byref(pcReturned))

        bufsize = pcbNeeded.value
        buffer = msvcrt.malloc(bufsize)
        winspool.EnumPrintersW(PRINTER_ENUM_LOCAL, Name, Level, buffer, bufsize,
                ctypes.byref(pcbNeeded), ctypes.byref(pcReturned))
        info = ctypes.cast(buffer, ctypes.POINTER(PRINTER_INFO_1))
        choices = [[None, "----"]]

        for i in range(pcReturned.value):
            choices.append([info[i].pName, info[i].pName])

        msvcrt.free(buffer)
        return choices
    except Exception as e:
        log("Error al obtener choices de impresoras:", level="error")
        log(str(e), level="error")
        return []

class ConfiguracionInicialForm(BootstrapForm):
    clave = forms.CharField(label="URL de acceso")
    token = forms.CharField(label="Token")

    def clean(self):
        from .functions import validar_token_admintotal

        data = self.cleaned_data
        configuracion_valida = validar_token_admintotal(
            clave=data.get("clave"),
            token=data.get("token"),
        )
        if configuracion_valida.get("status") != "success":
            mensaje_error = configuracion_valida.get("mensaje")
            if not mensaje_error:
                mensaje_error = "Hubo un error al validar la configuración"
            raise forms.ValidationError({
                "token": mensaje_error
            })

        data["redis_url"] = configuracion_valida.get("redis_url")
        data["nombre_agente"] = configuracion_valida.get("nombre_agente")
        data["razon_social"] = configuracion_valida.get("razon_social")
        data["rfc"] = configuracion_valida.get("rfc")
        return data

class ConfiguracionGeneralForm(BootstrapModelForm):
    nombre_impresora = forms.CharField(
        required=False, 
        label="Impresora"
    )
    configuraciones_impresora = forms.FileField(
        required=False, label="Archivo configuración de impresión"
    )

    class Meta:
        model = ConfiguracionGeneral
        fields = (
            "razon_social",
            "rfc",
            "direccion_calle",
            "direccion_numero_ext",
            "direccion_numero_int",
            "direccion_colonia",
            "direccion_cp",
            "telefono",
            "direccion_ciudad",
            "direccion_estado",
            "direccion_pais",
            "nombre_impresora",
            "ancho_papel_impresora_px",
        )
    
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
            
    
    def clean_configuraciones_impresora(self):
        archivo = self.cleaned_data.get("configuraciones_impresora")
        if archivo and archivo.name.split(".")[-1] != "dat":
            raise forms.ValidationError("La extensión del archivo debe ser .dat")
        return archivo

class ConfiguracionBiometricoForm(BootstrapModelForm):
    nt_license_trial = forms.BooleanField(label="Licencias de prueba", required=False)

    class Meta:
        model = ConfiguracionGeneral
        fields = [
            "nt_license_trial",
            "nt_biometric_serial_number",
        ]
    
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["nt_biometric_serial_number"].required = False
    

class ConfiguracionPinpadBanorteForm(BootstrapModelForm):
    banorte_puerto_pinpad = forms.ChoiceField(choices=[], label="Puerto serial")
    banorte_contactless = forms.BooleanField(label="¿Soporta tecnología contactless?", required=False)

    class Meta:
        model = ConfiguracionGeneral
        fields = (
            "banorte_contactless",
            "banorte_afiliacion",
            "banorte_usuario",
            "banorte_password",
            "banorte_puerto_pinpad",
        )

    def __init__(self, *args, **kwargs):
        from serial.tools import list_ports
        super().__init__(*args, **kwargs)

class ConfiguracionBasculaForm(BootstrapModelForm):
    bascula_puerto = forms.ChoiceField(choices=[], label="Puerto serial")

    class Meta:
        model = ConfiguracionGeneral
        fields = (
            "bascula_torrey",
            "bascula_puerto",
            "bascula_baudrate",
            "bascula_stopbits",
            "bascula_parity",
            "bascula_cmd",
            "bascula_byte_size",
        )

    def __init__(self, *args, **kwargs):
        from serial.tools import list_ports
        super().__init__(*args, **kwargs)
        self.fields["bascula_torrey"].required = False
        
class ConfiguracionPinpadSantanderForm(BootstrapModelForm):
    class Meta:
        model = ConfiguracionGeneral
        fields = (
            "santander_ambiente",
            "santander_usuario",
            "santander_password",
        )

