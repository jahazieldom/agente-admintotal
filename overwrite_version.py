import subprocess

def get_nueva_version(anterior):
    split_version = anterior.split(".")
    n = int(split_version.pop()) + 1
    split_version.append(n)
    return ".".join([str(x) for x in split_version])
    

version_anterior = open("version").read()
cambios_version = "Comentarios generados de manera automática overwrite_version.py"

print("")
print("*" * 25)
print(f"Versión a sobreescribir: {version_anterior}")
print("*" * 25)

print(f"Sobreescribiendo versión {version_anterior} tag en el repositorio....")
subprocess.call(["git", "commit", "-am", "Sobreescribir versión generada por overwrite_version.py"])
subprocess.call(["git", "push"])
subprocess.call(["git", "push", "--delete", "origin", f"v{version_anterior}"])
subprocess.call(["git", "tag", "--delete", f"v{version_anterior}"])

# publicando version actualizada
subprocess.call(["git", "tag", "-a", f"v{version_anterior}", "-m", cambios_version])
subprocess.call(["git", "push", "origin", "--tags"])
