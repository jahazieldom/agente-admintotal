import urllib3
import tempfile
import os
import zipfile
import subprocess
import shutil
import requests
import argparse

def get_last_tag_version():
    return requests.get("https://gitlab.com/api/v4/projects/17383229/repository/tags").json()[0]["name"].replace("v", "")

PROGRAM_FILES = 'C:\Program Files (x86)'

if not PROGRAM_FILES:
    PROGRAM_FILES = 'C:\Program Files'

AGENT_VERSION = get_last_tag_version()
INSTALLATION_PATH = os.path.join(PROGRAM_FILES, "Agente Admintotal")
PYTHON_PATH = os.path.join(PROGRAM_FILES, "Python37")
# PYTHON_PATH = os.path.join(PROGRAM_FILES, "Python310")

print(AGENT_VERSION)

if not PROGRAM_FILES:
    raise Exception(
        "La variable de entorno 'ProgramFiles' no esta definida"
    )

def run_as_admin():
    import ctypes, os
    try:
        is_admin = os.getuid() == 0
    except AttributeError:
        is_admin = ctypes.windll.shell32.IsUserAnAdmin() != 0
    return is_admin

def download_from_url(url, download_path):
    """
    docstring
    """
    try:
        http = urllib3.PoolManager()
        r = http.request('GET', url, preload_content=False)

        with open(download_path, 'wb') as out:
            chunk_size = 50000
            while True:
                data = r.read(chunk_size)
                if not data:
                    break
                out.write(data)

        r.release_conn()
        return True
    except Exception as e:
        print(e)
        
    return False

def download_python3(tmp_dir, arquitectura):
    """
    docstring
    """
    url = "https://www.python.org/ftp/python/3.7.9/python-3.7.9.exe"
    #url = "https://www.python.org/ftp/python/3.10.10/python-3.10.10.exe"

    if arquitectura == "amd64":
        url = "https://www.python.org/ftp/python/3.7.9/python-3.7.9-amd64.exe"
    
    filename = url.split("/")[-1]
    full_path = os.path.join(tmp_dir, filename)
    if download_from_url(url, full_path):
        return full_path

def install_python3(installer_path):
    """
    docstring
    """
    try:
        print(f"Instalador: {installer_path}")
        subprocess.call(
            [
                installer_path,
                "InstallAllUsers=1",
                "PrependPath=1",
                "Include_test=0",
                "Include_tcltk=0",
                "Include_doc=0",
                f"TargetDir={PYTHON_PATH}",
            ]
        )
        return True
    except Exception as e:
        print(e)

    return False

def download_source_code(tmp_dir):
    """
    docstring
    """
    url = f"https://gitlab.com/jahazieldom/agente-admintotal/-/archive/v{AGENT_VERSION}/agente-admintotal-v{AGENT_VERSION}.zip"
    filename = url.split("/")[-1]
    full_path = os.path.join(tmp_dir, filename)
    if download_from_url(url, full_path):

        with zipfile.ZipFile(full_path, "r") as zip_ref:
            zip_ref.extractall(tmp_dir)

        shutil.move(
            os.path.join(tmp_dir, f"agente-admintotal-v{AGENT_VERSION}"),
            INSTALLATION_PATH,
        )
        return INSTALLATION_PATH

def install_requirements(installation_dir):
    """
    docstring
    """
    try:
        subprocess.call(
            [
                os.path.join(PYTHON_PATH, "python.exe"),
                "-m",
                "pip",
                "install",
                "--upgrade",
                "pip",
            ]
        )

        subprocess.call(
            [
                os.path.join(PYTHON_PATH, "Scripts", "pip.exe"),
                "install",
                "-r",
                os.path.join(INSTALLATION_PATH, "requirements.txt"),
            ]
        )
        
        settings_file = os.path.join(INSTALLATION_PATH, "agente", "settings.py")
        if not os.path.exists(settings_file):
            print("Creando settings.py")
            shutil.copy(
                os.path.join(INSTALLATION_PATH, "agente", "settings_copy.py"), 
                settings_file
            )
    except Exception as e:
        print(e)

def start(arquitectura):
    """
    docstringdownload_source_code
    """
    PATH_CONFIG_IMPRESORA = os.path.join(
        INSTALLATION_PATH, "lib", "PDF-XChange Viewer Settings.dat"
    )
    PATH_CONFIG_IMPRESORA_BACKUP = None

    if os.path.exists(PATH_CONFIG_IMPRESORA):
        tmp_dir = tempfile.gettempdir()
        PATH_CONFIG_IMPRESORA_BACKUP = os.path.join(tmp_dir, "PDF-XChange Viewer Settings.dat")
        shutil.copyfile(PATH_CONFIG_IMPRESORA, PATH_CONFIG_IMPRESORA_BACKUP)

    try:
        os.chmod(INSTALLATION_PATH, 0o777)
        shutil.rmtree(INSTALLATION_PATH)
    except Exception as e:
        print(e)
        pass

    with tempfile.TemporaryDirectory() as tmp_dir:
        python_installed = True
        print(f"PYTHON_PATH={PYTHON_PATH}")
        if not os.path.exists(os.path.join(PYTHON_PATH, "python.exe")):
            print("Descargando python 3...")
            pyinstaller_exe = download_python3(tmp_dir, arquitectura=arquitectura)
            print("Instalando python 3...")
            python_installed = install_python3(pyinstaller_exe)

        if python_installed:
            print("Descargando repositorio...")
            download_source_code(tmp_dir)
            print("Configurando entorno...")
            install_requirements(INSTALLATION_PATH)
            
            # copiar a startup
            APP_DATA = os.environ.get("APPDATA")
            startup_dir = f"{APP_DATA}\Microsoft\Windows\Start Menu\Programs\Startup"

            if arquitectura == "x86":
                BOOTSTRAP_BAT_FILE = os.path.join(INSTALLATION_PATH, "bootstrap.bat")
            else:
                BOOTSTRAP_BAT_FILE = os.path.join(INSTALLATION_PATH, "bootstrap_amd64.bat")

            shutil.copy(
                BOOTSTRAP_BAT_FILE, 
                startup_dir
            )
            shutil.copy(
                BOOTSTRAP_BAT_FILE, 
                os.path.join(os.path.expanduser("~"), "Desktop", "Agente Admintotal.bat")
            )

            if PATH_CONFIG_IMPRESORA_BACKUP:
                shutil.copyfile(PATH_CONFIG_IMPRESORA_BACKUP, PATH_CONFIG_IMPRESORA)

            print("*" * 25)
            print("Proceso finalizado")
            print("*" * 25)

def parse_args():
    parser = argparse.ArgumentParser()
    arch_choices = ["x86", "amd64"]
    parser.add_argument("-a", "--arch", default="x86", choices=arch_choices, help="Arquitectura")
    return parser.parse_args()
        
if __name__ == "__main__":
    try:
        if not run_as_admin():
            raise Exception(
                "Es necesario ejecutar este proceso como "
                "Administrador (Click derecho -> Ejecutar como administrador)"
            )
        
        args = parse_args()

        print(f"Instalación {args.arch}")
        PROGRAM_FILES = 'C:\Program Files'
        print(PROGRAM_FILES)

        if args.arch == "x86":
            PROGRAM_FILES = 'C:\Program Files (x86)'

        if not PROGRAM_FILES:
            PROGRAM_FILES = 'C:\Program Files'

        INSTALLATION_PATH = os.path.join(PROGRAM_FILES, "Agente Admintotal")
        PYTHON_PATH = os.path.join(PROGRAM_FILES, "Python37")

        start(arquitectura=args.arch)
    except Exception as e:
        print("Error en la instalación:")
        print(e)
    finally:
        input("Precione Enter para salir...")
