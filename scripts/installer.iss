#define PYTHON_VERSION "3.7.9"
#define PYTHON_URL "https://www.python.org/ftp/python/{#PYTHON_VERSION}/python-{#PYTHON_VERSION}.exe"
#define AGENT_SCRIPT_URL "https://gist.githubusercontent.com/anonymous/XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX/raw/agent_script.py"

[Setup]
AppName=My Installer
AppVersion=1.0
DefaultDirName={pf}\MyInstaller
OutputDir=Output

[Files]
Source: "post_install.py"; DestDir: "{app}"

[Code]
function InitializeSetup: Boolean;
var
  PythonInstaller: string;
begin
  Result := True;

  { Descargar e instalar Python }
  PythonInstaller := ExpandConstant('{tmp}\python-{#PYTHON_VERSION}.exe');
  if not DownloadFile(PYTHON_URL, PythonInstaller) then
  begin
    MsgBox('Failed to download Python installer.', mbError, MB_OK);
    Result := False;
    Exit;
  end;
  
  if not Exec(PythonInstaller, '/quiet InstallAllUsers=1 PrependPath=1 Include_test=0 Include_tcltk=0 Include_doc=0 TargetDir={pf}\Python37', '', SW_SHOW, ewWaitUntilTerminated, ErrorCode) then
  begin
    MsgBox('Failed to install Python.', mbError, MB_OK);
    Result := False;
    Exit;
  end;

  { Ejecutar el script de Python }
  if not Exec(ExpandConstant('{sys}\cmd.exe'), '/C "{app}\post_install.py"', '', SW_SHOW, ewWaitUntilTerminated, ErrorCode) then
  begin
    MsgBox('Failed to execute post_install.py.', mbError, MB_OK);
    Result := False;
    Exit;
  end;
end;
