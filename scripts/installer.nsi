!define PYTHON_VERSION "3.7.9"
!define PYTHON_URL "https://www.python.org/ftp/python/${PYTHON_VERSION}/python-${PYTHON_VERSION}.exe"

Outfile "Installer.exe"
RequestExecutionLevel admin

Page instfiles

Section
    SetOutPath $TEMP

    ; Descargar e instalar Python
    nsExec::Exec 'powershell -Command "(New-Object System.Net.WebClient).DownloadFile('${PYTHON_URL}', '$TEMP\python-${PYTHON_VERSION}.exe')"'
    ExecWait "$TEMP\python-${PYTHON_VERSION}.exe InstallAllUsers=1 PrependPath=1 Include_test=0 Include_tcltk=0 Include_doc=0 TargetDir=$PROGRAMFILES32\Python37"

    ; Ejecutar el script de Python
    ExecWait '"$SYSDIR\cmd.exe" /C "$INSTDIR\installer.py"'

SectionEnd
