from systray import SysTrayIcon
import threading
import webbrowser
import defs

thread_runserver = None
thread_daemon = None

stop_event_runserver = None
stop_event_daemon = None

def log(msg, level="INFO", logger_name="agente"):
    """
    Helper function
    """
    import logging
    import datetime
    
    logger = logging.getLogger((logger_name))
    level = level.lower()
    time_info = datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S")
    log_msg = f"[{time_info}] {level} - {msg}"
    getattr(logger, level)(log_msg)
    print(log_msg)

def configuracion(systray):
    log("Abriendo url http://localhost:8000")
    webbrowser.open_new('http://localhost:8000')
    log("Fin")

def logs(systray):
    log("Abriendo url http://localhost:8000/log/agente/")
    webbrowser.open_new('http://localhost:8000/log/agente/')
    log("Fin")

def get_menu_options(running=False):
    if not running:
        return (
            ("Iniciar servicios", None, iniciar),
            # ("Configuración", None, configuracion),
        )
    
    return  (
        ("Detener servicios", None, detener),
        ("Configuración", None, configuracion),
        ("Logs", None, logs),
    )

def iniciar(systray):
    global thread_runserver
    global thread_daemon

    global stop_event_runserver
    global stop_event_daemon

    # Crear eventos para indicar a los procesos que deben finalizar
    stop_event_runserver = threading.Event()
    stop_event_daemon = threading.Event()

    thread_runserver = threading.Thread(
        target=defs.runserver, 
        args=[stop_event_runserver],
    )
    thread_runserver.start()

    thread_daemon = threading.Thread(
        target=defs.start_daemon, 
        args=[stop_event_daemon],
    )
    thread_daemon.start()

    systray.update(menu_options=get_menu_options(True))

def detener(systray):
    on_quit_callback(systray)
    systray.update(menu_options=get_menu_options())

def on_quit_callback(systray):
    if thread_runserver:
        stop_event_runserver.set()

    if thread_daemon:
        stop_event_daemon.set()

systray = SysTrayIcon(
    "systray.ico", 
    "Agente Admintotal", 
    get_menu_options(), 
    on_quit=on_quit_callback
)

systray.start()
