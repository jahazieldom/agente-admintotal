import os
import sys
import subprocess
from agente.base_settings import BASE_DIR
import signal

PYTHON_EXE = sys.executable
MANAGE_PY = os.path.join(BASE_DIR, "manage.py")

def runserver(stop_event):
    """
    Wrapper para el comando que iniciará el servidor web.
    El parámetro stop_event debe ser un objeto threading.Event
    """
    cmd = [PYTHON_EXE, MANAGE_PY, "runserver"]
    proc = subprocess.Popen(cmd, creationflags=subprocess.CREATE_NEW_PROCESS_GROUP)
    while True:
        if stop_event.is_set():
            proc.send_signal(signal.CTRL_BREAK_EVENT)
            proc.kill()
            break

def start_daemon(stop_event):
    """
    Wrapper para el comando que iniciará el agente.
    El parámetro stop_event debe ser un objeto threading.Event
    """
    cmd = [PYTHON_EXE, MANAGE_PY, "agente", "iniciar"]
    proc = subprocess.Popen(cmd, creationflags=subprocess.CREATE_NEW_PROCESS_GROUP)
    while True:
        if stop_event.is_set():
            proc.send_signal(signal.CTRL_BREAK_EVENT)
            proc.kill()
            break
