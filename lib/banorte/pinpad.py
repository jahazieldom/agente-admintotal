from agente.settings import BASE_DIR, LOCAL_DEVELOPMENT
import os
from utils.functions import log, localtime
import clr


"""
(MODE) = PRD (Producción), la transacción es procesada en modo real.
(MODE) = AUT (Autorización), modo de simulación, la transacción enviada en este modo siempre es aceptada.
(MODE) = DEC (Declinado), modo de simulación, la transacción enviada en este modo siempre es rechazada.
(MODE) = RND (Random), modo de simulación, la transacción enviada en este modo es aceptada o rechazada aleatoriamente.
"""
PINPAD_ENV = "PRD" # "PRD"
"""
if LOCAL_DEVELOPMENT:
    PINPAD_ENV = "AUT"
"""

def hashtable_to_dict(ht):
    ht_dict = {}
    for key in ht.get_Keys():
        ht_dict[key] = ht.get_Item(key)
    return ht_dict

instances = {}

def singleton(class_):
    def getinstance(*args, **kwargs):
        if class_ not in instances:
            print("Guardando instancia en memoria")
            instances[class_] = class_(*args, **kwargs)
        return instances[class_]

    return getinstance

@singleton
class PinpadBanorte:
    """
    __DOCS__
    """

    def __init__(self, *args, **kwargs):
        """
        __DOCS__
        """
        from . import DLL_API_VERSIONS
        import platform

        DLL_API_VERSIONS = DLL_API_VERSIONS.get(platform.architecture()[0])
        
        banorte_base_dir = os.path.join(BASE_DIR, "lib", "banorte")
        dll_file_name = DLL_API_VERSIONS.get("INT")

        if kwargs.get("contactless"):
            dll_file_name = DLL_API_VERSIONS.get("CTLS")
        
        dll_full_path = os.path.join(banorte_base_dir, dll_file_name)
        clr.AddReference(dll_full_path)

        self.contactless = bool(kwargs.get("contactless"))
        self.modelo_pinpad = kwargs.get("modelo_pinpad", "Vx820")
        self.velocidad = "19200"
        self.paridad = "N"
        self.bits_paro = "1"
        self.bits_datos = "8"
        self.puerto = kwargs.get("puerto")
        self.afiliacion = kwargs.get("afiliacion")
        self.usuario = kwargs.get("usuario")
        self.password = kwargs.get("password")

        if not self.afiliacion:
            raise Exception("La afiliación no fué especificada")

        if not self.usuario:
            raise Exception("El usuario no fué especificado")
            
        if not self.password:
            raise Exception(f"La contraseña del usuario {self.usuario} no fué especificada")
    
    @property
    def instance(self):
        return self._get_instance()

    def _get_instance(self):
        from System.Collections import Hashtable
        import Banorte


        if getattr(self, "instance_obj", None):
            return self.instance_obj

        configuracion_puerto = Hashtable()
        info = Hashtable()

        configuracion_puerto['PORT'] = self.puerto
        configuracion_puerto['BAUD_RATE'] = self.velocidad
        configuracion_puerto['PARITY'] = self.paridad
        configuracion_puerto['STOP_BITS'] = self.bits_paro
        configuracion_puerto['DATA_BITS'] = self.bits_datos

        self.instance_obj = Banorte.PinPad.Vx820Segura("EN")
        try:
            log(f"Inicializando dispositivo {self.puerto} ...")
            self.instance_obj.inicializarDispositivo(configuracion_puerto)
            self.instance_obj.obtenerInformacion(info)
            self.numero_serie = info.get_Item('SERIAL_NUMBER')
            # log(f"Dispositivo inicializado correctamente, serie {self.numero_serie}")
            return self.instance_obj
        except Exception as e:
            self.liberar_dispositivo(self.instance_obj)
            raise Exception(e)

    def cargar_llave(self):
        """
        __DOCS__
        """     
        from System.Collections import Hashtable
        import Banorte

        entrada = Hashtable()
        salida = Hashtable()
        selector_salida = Hashtable()

        instance = self.instance
        
        instance.obtenerSelector(selector_salida)
        selector = selector_salida["SELECTOR"]

        entrada["CMD_TRANS"] = "GET_KEY"
        entrada["USER"] = self.usuario
        entrada["PASSWORD"] = self.password
        entrada["MERCHANT_ID"] = self.afiliacion
        entrada["SERIAL_NUMBER"] = self.numero_serie
        entrada["CONTROL_NUMBER"] = f"CARGALLAVE{self.usuario}"
        entrada["SELECTOR"] = selector
        entrada["RESPONSE_LANGUAGE"] = "EN"
        entrada["BANORTE_URL"] = "https://medios.pagosbanorte.com/InterredesSeguro"

        try:
            log("Solicitando llave maestra...")
            Banorte.ConectorBanorte.sendTransaction(entrada, salida)
        except Exception as e:
            self.liberar_dispositivo(instance)
            raise e
        
        entrada = hashtable_to_dict(entrada)
        salida = hashtable_to_dict(salida)
        resultado = salida.get('PAYW_RESULT')
        codigo = salida.get('PAYW_CODE')

        if resultado != "A":
            log("Error al cargar llave maestra", "error")
            log(f"Entrada:", "error")
            log(f"{entrada}", "error")
            log(f"Salida:", "error")
            log(f"{salida}", "error")
            
            self.liberar_dispositivo(instance)
            raise Exception(
                f"Error al cargar llave maestra PAYW_RESULT={resultado} "
                f"PAYW_CODE={codigo} "
                f"TEXT={salida.get('TEXT')} "
            )
        
        log("Cargando llave maestra...")
        entrada = Hashtable()
        entrada["SERIAL_NUMBER"] = self.numero_serie
        entrada["MASTER_KEY"] = salida.get('TEXT')
        
        instance.loadMasterKey(entrada)
        self.liberar_dispositivo(instance)
        log("Llave maestra cargada correctamente")
        return True
    
    def get_datos_verificacion(self, respuesta_verificacion, salida=None):
        from System.Collections import Hashtable

        if not salida:
            salida = Hashtable()

        respuesta_keys = [
            "TIPO_TRANSACCION",
            "REFERENCE",
            "CARD_NUMBER",
            "AMOUNT",
            "PAYW_CODE",
            "PAYW_RESULT",
            "AUTH_RESULT",
            "AUTH_CODE",

            "FECHA_BANORTE",
            "FECHA_PROSA",
            "FECHA_PROSA_SALIDA",
            "Date",

            "ISSUING_BANK",
            "CARD_BRAND",
            "CARD_TYPE",
            "DESCONOCIDO",
            "TAG9F34",
        ]
        # son 17 valores en la lista
        for i, v in enumerate(respuesta_verificacion.split("|")):
            if v == "null":
                v = None

            if len(respuesta_keys) - 1 >= i:
                salida.set_Item(respuesta_keys[i], v)
            else:
                break
        return salida
    
    def verificar_transaccion(self, referencia):
        from System.Collections import Hashtable
        
        instance = self.instance

        entrada = Hashtable()
        salida = Hashtable()
        entrada["CMD_TRANS"] = "VERIFY"
        entrada["USER"] = self.usuario
        entrada["PASSWORD"] = self.password
        entrada["MERCHANT_ID"] = self.afiliacion
        entrada["TERMINAL_ID"] = self.numero_serie
        entrada["CONTROL_NUMBER"] = referencia
        entrada["MODE"] = PINPAD_ENV # modo


        entrada["RESPONSE_LANGUAGE"] = "EN"
        entrada["BANORTE_URL"] = "https://medios.pagosbanorte.com/InterredesSeguro"

        try:
            instance.processTransaction(entrada, salida)
        except Exception as e:
            log(e, "error")
            
            error = str(e)
            if hasattr(e, "get_Message"):
                error = e.get_Message()
            
            return {
                "status": "error",
                "mensaje": f"Transacción no procesada: {error}",
            }

        respuesta = salida.get_Item("TEXT")
        if "transaction does not exist" in (respuesta or "").lower():
            return {
                "status": "error",
                "mensaje": f"Transacción no procesada: {respuesta}",
            }
        
        salida = self.get_datos_verificacion(respuesta, salida=salida)
        salida.set_Item("CONTROL_NUMBER", referencia)
        fecha_banorte = salida.get_Item("Date")

        if salida.get_Item("PAYW_CODE") == "C":
            salida.set_Item("PAYW_RESULT", "A")
        else:
            salida.set_Item("PAYW_RESULT", "XX")

        if fecha_banorte:
            try:
                import datetime
                import pytz

                fecha_banorte = datetime.datetime.strptime(fecha_banorte, "%Y%m%d %H:%M:%S.%f")
                tz = pytz.timezone('America/Mexico_City')
                fecha_banorte = tz.localize(fecha_banorte).isoformat()
                salida.set_Item("Date", fecha_banorte)
            except Exception as e:
                log(e, "error")

        salida = hashtable_to_dict(salida)
        data = self._get_resultado(salida)
        autorizacion = data.get("autorizacion")
        status = "success"
        if not autorizacion:
            status = "error"
        
        return {
            "status": status,
            "data": data,
            "mensaje": data.get("mensaje"),
        }
        
    def realizar_cobro(self, monto, referencia):
        from System.Collections import Hashtable
        
        instance = self.instance
        entrada = Hashtable()
        salida = Hashtable()

        monto = str(monto)

        log(f"Iniciando transacción {referencia} por el monto ${monto} (modo {PINPAD_ENV})")



        entrada["CMD_TRANS"] = "AUTH"
        entrada["USER"] = self.usuario
        entrada["PASSWORD"] = self.password
        entrada["MERCHANT_ID"] = self.afiliacion
        entrada["TERMINAL_ID"] = self.numero_serie
        entrada["CONTROL_NUMBER"] = referencia
        entrada["MODE"] = PINPAD_ENV # modo


        entrada["AMOUNT"] = monto
        entrada["RESPONSE_LANGUAGE"] = "EN"
        # entrada["BANORTE_URL"] = "https://medios.pagosbanorte.com/InterredesSeguro"

        # recomendación de actualización de la url por JORGE LUIS GONZALEZ PUENTE
        entrada["BANORTE_URL"] = "https://via.pagosbanorte.com/InterredesSeguro"

        es_verificacion = False
        error = None

        try:
            instance.processTransaction(entrada, salida)
        except Exception as e:
            error = str(e)

            if hasattr(e, "get_Message"):
                error = e.get_Message()

            errores_verificables = [
                # Falla en recepción de dispositivo; verifique conexiones.
                "ERR010", 

                # No se recibió respuesta de la PIN Pad en el tiempo máximo permitido
                "ERR011",

                # Respuesta inválida de PIN Pad; verifique versión y conexiones.
                "ERR012",

                # Un componente externo produjo una excepción. en std._Xout_of_range(SByte* ) en 
                # std.basic_string<char,std::char_traits<char>,std::allocator<char> >.
                # at(basic_string<char\,std::char_traits<char>\,std::allocator<char> 
                # >* , UInt32 _Off) en EndTransaction.readNotify(EndTransaction
                "External component has thrown an exception",
                "Un componente externo produjo una excepción",
            ]

            for err in errores_verificables:
                if f"[{err}]" in error:
                    log(f"Verificando transacción: {referencia}")
                    salida = self.verificar_transaccion(referencia)
                    es_verificacion = True
                    break

                if f"{err}".lower() in error.lower():
                    log(f"Verificando transacción: {referencia}")
                    salida = self.verificar_transaccion(referencia)
                    es_verificacion = True
                    break
            
        self.liberar_dispositivo(instance)

        if isinstance(salida, Hashtable):
            salida = hashtable_to_dict(salida)

        if salida.get("status") == "error" or (not salida and not es_verificacion): 
            error = salida.get("mensaje") or error
            return {
                "status": "error",
                "mensaje": f"Transacción no procesada: {error}",
                "imprimir_comprobante": False,
            }
        
        entrada = hashtable_to_dict(entrada)
        if not es_verificacion:
            codigo_banorte = salida.get("PAYW_RESULT")
            
            log(f"Entrada de transacción {referencia}:")
            log(entrada)

            log(f"Salida de transacción {referencia}:")
            log(salida)

            data = self._get_resultado(salida)
            mensaje = salida.get("TEXT")
        else:
            data = salida.get("data")
            codigo_banorte = salida["data"]["resultado"]
            mensaje = "Transacción procesada correctamente"
        
        data["monto"] = monto

        status = "success"
        if codigo_banorte != "A":
            status = "error"

        return {
            "status": status,
            "mensaje": mensaje,
            "data": data,
            "raw": salida,
            "imprimir_comprobante": True,
            "consulta_verificacion": es_verificacion, 
        }
    
    def cancelar_transaccion(self, referencia, monto):
        """
        (MODE) = PRD (Producción), la transacción es procesada en modo real.
        (MODE) = AUT (Autorización), modo de simulación, la transacción enviada en este modo siempre es aceptada.
        (MODE) = DEC (Declinado), modo de simulación, la transacción enviada en este modo siempre es rechazada.
        (MODE) = RND (Random), modo de simulación, la transacción enviada en este modo es aceptada o rechazada aleatoriamente.
        """
        from System.Collections import Hashtable

        instance = self.instance
        entrada = Hashtable()
        salida = Hashtable()


        log(f"Cancelando transacción {referencia}")

        entrada["CMD_TRANS"] = "VOID"
        entrada["USER"] = self.usuario
        entrada["PASSWORD"] = self.password
        entrada["MERCHANT_ID"] = self.afiliacion
        entrada["TERMINAL_ID"] = self.numero_serie
        entrada["REFERENCE"] = referencia
        entrada["CONTROL_NUMBER"] = f"CAN-{referencia}"
        entrada["MODE"] = PINPAD_ENV # modo


        entrada["RESPONSE_LANGUAGE"] = "EN"
        entrada["BANORTE_URL"] = "https://medios.pagosbanorte.com/InterredesSeguro"

        try:
            instance.processTransaction(entrada, salida)
            self.liberar_dispositivo(instance)
        except Exception as e:
            self.liberar_dispositivo(instance)
            log(e, "error")
            
            error = str(e)
            if hasattr(e, "get_Message"):
                error = e.get_Message()
            
            return {
                "status": "error",
                "mensaje": f"Transacción no procesada: {error}",
                "imprimir_comprobante": False,
            }
        
        entrada = hashtable_to_dict(entrada)
        salida = hashtable_to_dict(salida)
        codigo_banorte = salida.get("PAYW_RESULT")
        
        log(f"Entrada de transacción {referencia}:")
        log(entrada)

        log(f"Salida de transacción {referencia}:")
        log(salida)

        data = self._get_resultado(salida)
        data["monto"] = monto

        status = "success"
        if codigo_banorte != "A":
            status = "error"

        return {
            "status": status,
            "mensaje": salida.get("TEXT"),
            "data": data,
            "raw": salida,
            "imprimir_comprobante": True,
        }
                
    def _get_resultado(self, salida):
        numero_tarjeta = salida.get("CARD_NUMBER")
        expiracion_tarjeta = salida.get("CARD_EXP")

        if expiracion_tarjeta:
            expiracion_tarjeta = expiracion_tarjeta[2:] + "/" + expiracion_tarjeta[:2]

        if numero_tarjeta:
            numero_tarjeta = ["*" for x in salida.get("CARD_NUMBER")[:-4]]
            numero_tarjeta += salida.get("CARD_NUMBER")[-4:]
            numero_tarjeta = "".join(numero_tarjeta)

        return {
            "referencia": salida.get("REFERENCE"),
            "afiliacion": salida.get("MERCHANT_ID"),
            "modo_entrada": salida.get("ENTRY_MODE"),
            "terminal": self.numero_serie,
            "autorizacion": salida.get("AUTH_CODE"),
            "numero_control": salida.get("CONTROL_NUMBER"),
            "fecha_respuesta": salida.get("Date"),
            "mensaje": salida.get("TEXT"),

            "captura_nip": bool(int(salida.get("PIN_ENTRY", "0"))),
            "banco_emisor": salida.get("ISSUING_BANK"),
            "tarjetahabiente": salida.get("CARD_HOLDER"),
            "expiracion_tarjeta": expiracion_tarjeta,
            "numero_tarjeta": numero_tarjeta,
            "marca_tarjeta": salida.get("CARD_BRAND"),
            "tipo_tarjeta": salida.get("CARD_TYPE"),
            "apn": salida.get("APN"),
            "al": salida.get("AL"),
            "al": salida.get("AL"),
            "aid": salida.get("AID"),
            "tsi": salida.get("TSI"),
            "tvr": salida.get("TVR"),
            "emv_tags": salida.get("EMV_TAGS"),
            "resultado": salida.get("PAYW_RESULT"),
            "declinado_chip": salida.get("CHIP_DECLINED") == "1",
            "declinado_emv": salida.get("EMV_RESULS") == "D",
            "fecha": localtime().strftime("%d/%m/%Y %H:%M"),
        }

    def liberar_dispositivo(self, instance):
        global instances
        
        instances = {}

        try:
            instance.releaseDevice()
        except Exception as e:
            pass
        