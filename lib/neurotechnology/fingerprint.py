# from agente.settings import BASE_DIR
from utils.functions import log, localtime
import os
import clr
import time
from django.conf import settings
import base64
import time


dlls_dir = os.path.join(
    os.path.dirname(os.path.dirname(os.path.abspath(__file__))), "neurotechnology"
)

clr.AddReference(os.path.join(dlls_dir, "dlls", "Neurotec.dll"))
clr.AddReference(os.path.join(dlls_dir, "dlls", "Neurotec.Licensing.dll"))
clr.AddReference(os.path.join(dlls_dir, "dlls", "Neurotec.Biometrics.dll"))
clr.AddReference(os.path.join(dlls_dir, "dlls", "Neurotec.Biometrics.Client.dll"))
clr.AddReference(os.path.join(dlls_dir, "dlls", "Neurotec.Devices.dll"))
clr.setPreload(True)

import Neurotec
import Neurotec.Licensing
import Neurotec.Biometrics as NB
import Neurotec.Biometrics.Client as NBC
import Neurotec.Devices as ND

NBO = Neurotec.Biometrics.NBiometricOperations

instances = {}

def singleton(class_):
    def getinstance(*args, **kwargs):
        if class_ not in instances:
            instances[class_] = class_(*args, **kwargs)
        return instances[class_]

    return getinstance


@singleton
class FingerPrintNeurotec:
    def __init__(self, *args, **kwargs):
        biometric_serial_number = kwargs.get("biometric_serial_number")
        self.trial = kwargs.get("trial", True)
        self.output_image_path = kwargs.get("output_image_path")
        self.errores_licencias = []

        if not self.output_image_path:
            self.output_image_path = os.path.join(settings.MEDIA_ROOT, "fingerprint.png")
            
            if not os.path.exists(settings.MEDIA_ROOT):
                os.mkdir(settings.MEDIA_ROOT)

        self.output_template = self.output_image_path.split(".")[0]


        self.set_license()

        self.en_espera_lectura = False
        self._biometricClient = NBC.NBiometricClient()
        self._biometricClient.set_UseDeviceManager(True)
        self._biometricClient.set_BiometricTypes(NB.NBiometricType.Finger)
        self._biometricClient.Initialize()

        self._deviceManager = self._biometricClient.get_DeviceManager()
        self._deviceManager.Initialize()
        
        if biometric_serial_number:
            self.set_biometric(biometric_serial_number)

    def set_license(self):
        log("Obteniendo licencias...")

        try:
            license_components = [
                "Biometrics.FingerExtraction",
                "Devices.FingerScanners",
                "Biometrics.FingerMatching",
                "Biometrics.FingerSegmentation",
                "Biometrics.FingerQualityAssessmentBase",
                "Images.WSQ",
            ]
            Neurotec.Licensing.NLicenseManager.TrialMode = self.trial
            errores = []

            for component in license_components:
                res = Neurotec.Licensing.NLicense.ObtainComponents(
                    "/local", 5000, component
                )
                if not res:
                    log(f"Error al obtener licencia del componente {component}: {res}", level="ERROR")
                    # errores.append(component)
            
            if not errores:
                log("Configuración de licencias finalizado")
            else: 
                self.errores_licencias = errores
        except Exception as e:
            global instances
            instances = {}
            print(e)

    def set_biometric(self, serial_number):
        devices = self.get_devices()
        if not devices:
            log("No se encontraron dispositivos biométricos", level="error")
            return None

        selected_device = None
        for device in devices:
            if device.get_SerialNumber() == serial_number:
                selected_device = device

        if not selected_device:
            log(
                f"No se encontro ningún biométrico con numero de serie: {serial_number}",
                level="error",
            )

        self._biometricClient.FingerScanner = selected_device

    def get_devices(self, as_tuple=False):
        scanners = []
        for device in self._deviceManager.Devices:
            if as_tuple:
                scanners.append((device.get_SerialNumber(), device.get_DisplayName()))
            else:
                scanners.append(device)

        return scanners

    def finger_scan(self):
        if self.errores_licencias:
            licencias = ", ".join(self.errores_licencias)
            return {
                "status": "error",
                "mensaje": f"Hubo un problema al obtener las siguientes licencias: {licencias}",
            }

        if self._biometricClient.FingerScanner == None:
            err = "No hay ningún lector biométrico seleccionado"
            log(err, level="error")
            return {
                "status": "error",
                "mensaje": err
            }
        
        else:
            log("-" * 15, level="DEBUG")
            log(f"Dispositivo: {self._biometricClient.FingerScanner.get_DisplayName()}", level="DEBUG")
            log(f"N/S: {self._biometricClient.FingerScanner.get_SerialNumber()}", level="DEBUG")
            log("-" * 15, level="DEBUG")

            if self.en_espera_lectura:
                self.cancelar_operacion()

            self._subjectFinger = NB.NFinger()
            self._subject = NB.NSubject()
            self._subject.Fingers.Add(self._subjectFinger)
            self._subjectFinger.CaptureOptions = Neurotec.Biometrics.NBiometricCaptureOptions.Stream
            self._subjectFinger.PropertyChanged += self._on_attr_props_changed

            #self._biometricClient.set_FingersReturnBinarizedImage(True)
            log("Iniciando tarea de lectura...", level="DEBUG")
            task = self._biometricClient.CreateTask(NBO.Capture | NBO.CreateTemplate, self._subject)
            
            self._biometricClient.PerformTask(task)
            self.en_espera_lectura = False
            bytes_arr = bytes()

            if self._subjectFinger.Status == NB.NBiometricStatus.Ok:
                log("Tarea de lectura finalizada", level="DEBUG")
                # guardamos la imagen y el template en un archivo temportal
                bytes_arr = bytes(self._subject.GetTemplateBuffer().ToArray())

                with open(self.output_template, "wb") as file:
                    file.write(bytes_arr)
                
                self._subjectFinger.Image.Save(self.output_image_path)
                with open(self.output_image_path, "rb") as image_file:
                    # Leemos la imagen que acabamos de generar y la encodeamos 
                    # en base64 para mostrarla en el punto de venta y posteriormente
                    # enviarla a validar a la API de moderna
                    base64_image = base64.b64encode(image_file.read()).decode()

                return {
                    "base64_image": base64_image,
                    "base64_template":  base64.b64encode(bytes_arr).decode(),
                    "local_path": self.output_image_path,
                }
            
            # forzamos la creación de nueva instancia en el siguiente intento
            global instances
            instances = {}
            return {
                "status": "error",
                "mensaje": (
                    f"[Status {self._subjectFinger.Status}] Error de comunicación con el lector de huella, "
                    "revise que este conectado correctamente, si el problema persiste puede intentar "
                    "reiniciar el servicio del agente en su equipo y re-conectando el "
                    "lector de huella."
                )
            }
    
    def _on_attr_props_changed(self, sender, event):
        log(event.PropertyName)

        if event.PropertyName == "Status":
            log(self._subjectFinger.Status)
            if self._subjectFinger.Status == NB.NBiometricStatus.Ok:
                return
            
        if event.PropertyName == "Image" and not self.en_espera_lectura:
            log("Dispositivo listo para la lectura...")
            self.en_espera_lectura = True


    def cancelar_operacion(self):
        log("Cancelando lectura de huella...")
        self._biometricClient.Cancel()

